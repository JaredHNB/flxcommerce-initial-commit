﻿// Navigation open / close states



$(document).ready(function () {
    $('.searcher').on('click', function () {
        $('.searchercontainer').toggleClass('opennav');
        $("#keywords").focus();
    });
});


// Navigation

$('.haschildren').click(function () {
        if ($(this).hasClass('selected')) {
            $('.haschildren').removeClass('selected');
        } else {
            $('.haschildren').removeClass('selected');
            $(this).toggleClass('selected');
        }
    
});


// Full screen slider image
var $header = $('header');
var $item = $('.carousel-item');
var $wHeight = $(window).height();
var $wHeight = $(window).height() - $header.height(); // Calculate image but deduct the height of the header element
// $('#carouselExampleIndicators').css('margin-top', $header.height()); // Add header height as top margin to makeup for fixed element



$item.eq(0).addClass('active');
$item.height($wHeight);
$item.addClass('full-screen');

$('.carousel img').each(function () {
    var $src = $(this).attr('src');
    var $color = $(this).attr('data-color');
    $(this).parent().css({
        'background-image': 'url(' + $src + ')',
        'background-color': $color
    });
    $(this).remove();
});



// Slider settings

$('#slider').bxSlider({
    auto: true,
    autoControls: false,
    stopAutoOnClick: true,
    mode: 'fade',
    pagerCustom: '#bx-pager'
    //    touchEnabled: true,
    //    swipeThreshold: 50,
    //    oneToOneTouch: true,
    //    preventDefaultSwipeX: true,
    //    preventDefaultSwipeY: false
});




// Slick Carousel
$(document).ready(function () {
    $('.autoplay').slick({
        centerMode: true,
        centerPadding: '240px',
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
            {
                breakpoint: 1400,
                settings: {
                    arrows: true,
                    centerMode: true,
                    centerPadding: '140px',
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 780,
                settings: {
                    arrows: true,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 1
                }
            }
        ]
    });
});




var fixed = document.getElementById('navbarSide');

fixed.addEventListener('touchmove', function(e) {

        e.preventDefault();

}, false);


$(document).ready(function () {
    $('.first-button').on('click', function () {
    $('.animated-icon1').toggleClass('open');
    });
});



$('.galleryimagelink').each(function (i) {
    setTimeout(function () {
        $('.galleryimagelink').eq(i).addClass('inview');
    }, 50 * (i + 1));
    
});

    // Slide in elements
	/*
$(document).ready(function () {
        AOS.init({
            easing: 'ease-out-back',
            duration: 300
        });
});
*/

// Slick Carousel

    $('.autoplay2').slick({
        infinite: true
});


$('.autoplay3').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    centerMode: false,
    centerPadding: '20px'
});

    // Image modal

    
    $(document).ready(function () {
        $('.galleryimage img').on('click', function () {
            var name = $(this).attr('alt');
            var src = $(this).attr('data-content');

            var img = '<img src="' + src + '" alt="' + name + '" />';

            //start of new code new code

            var html = '';
            html += img;

            $('#example .modal-body').html(img);
        });
    });
    
	
var _isOS = navigator.userAgent.match(/(iPod|iPhone|iPad)/);

if (_isOS) {
  $('body').addClass('is-os');
} 
/*
if (iosVersion >= 7) {
    $(document).scroll(function() {
        $('#parallax-container').css('background-position', '0px ' + $(document).scrollTop() + 'px');
    });
}
*/