﻿flxCommerceApp.factory("$parentScope", function ($window) {
    return {

        getParentOrderScope: function () {
            return $window.parent.angular.element($window.frameElement).scope();
        }
    }
});