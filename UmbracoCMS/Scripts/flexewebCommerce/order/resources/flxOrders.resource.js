﻿flxCommerceApp.factory("flxOrdersResource", function ($http) {
    return {

        getAllOrders: function () {
            return $http.get("/api/OrderWebService/GetAllOrders")
        },
        getOrderById: function (orderId) {
            return $http.get("/api/OrderWebService/GetOrderById?orderId=" + orderId)
        },
        getCurrentCustomerOrders: function () {
            return $http.get("/api/OrderWebService/GetCurrentCustomerOrders")
        },
        getCurrentCustomerOrder: function () {
            return $http.get("/api/OrderWebService/GetCurrentCustomerOrder");
        },
        deleteOrderLine: function (data) {
            return $http.post("/api/OrderWebService/DeleteOrderLine", data);
        },
        updateOrderLine: function (data) {
            return $http.post("/api/OrderWebService/UpdateOrderLineQuantity", data);
        },
        addOrderline: function (data) {
            return $http.post("/api/OrderWebService/AddOrderLine", data);
        }

    }
});