﻿
function callback(obj) {
    if (obj && obj.token) {
        var tokenData = { "token": obj.token }
        $.ajax({
            url: "/api/PaymentGatewayWebService/RegisterTransactionWithWorldPay",
            dataType: 'json',
            type: 'post',
            contentType: 'application/json',
            data: JSON.stringify(tokenData)
        }).done(function (data) {        
            window.location.href = data;
        });

    }
}


window.onload = function () {
    Worldpay.setClientKey('T_C_0a2cf641-3272-4a7b-a9ff-2daaa20f37f1');
    Worldpay.reusable = false;
    Worldpay.useTemplate('payment-form', 'my_payment_section', 'inline', callback);
}