//Global Variables
var attributes = [];
var sizes = [];
var colours = [];
var count = 0;

$(document).ready(function () {
    var productAttributes = new Object;


    function selectColour(element) {

        isValid = true;


        //Remove all size related information
        RemoveAllHighlights(element);
        PreventAddToBasket();

        element.addClass("selectedAttribute");

        //Set global ProductId var
        window['productId'] = element.parents("#attributes").attr("data-productId");

        //Add selected Colour to global Colour var
        window['colour'] = element.attr("id");

        //Add Sizes to global Sizes array based on selected Colour
        GetSizes(attributes, window['colour']);
        //Remove OutofStock class from elements that match the array
        AddSizeOpacity(sizes);

        //Add attribute vars to ProductAttributes object 
        productAttributes["colour"] = getAttVal(window['colour']);

        if ($("a[name=size]").length > 0){
            productAttributes["size"] = getAttVal(window['size']);
        }

        if (isValid == true) {
            //Call GetInventory method
            GetInventoryStatus(productAttributes);
        }
    }


    //Add Attribute
    $("a[name=colour]").click(function (e) {

        selectColour($(this));

    });
    //Add Attribute
    $("a[name=size]").click(function (e) {

        if (!$(this).parent().hasClass("outOfStock")) {
            isValid = true;

            $(this).parent().siblings().removeClass("selectedAttribute");

            $(this).parent().addClass("selectedAttribute");

            //Set global ProductId var
            window['productId'] = $(this).parents("#attributes").attr("data-productId");

            //Add selected size to global Size var
            window['size'] = $(this).attr("id");

            //Add Colours to gloabl Colours array based on selected Size
            GetColours(attributes, window['size']);


            //Add attribute vars to ProductAttributes object 
            if ($("a[name=colour]").length > 0) {
                productAttributes["colour"] = getAttVal(window['colour']);
            }
            else {
                delete productAttributes.colour;
            }

            productAttributes["size"] = getAttVal(window['size']);

            if (isValid == true) {
                //Call GetInventory method
                GetInventoryStatus(productAttributes);
            }
        }
    });

    $("#flxLogo").click(function (e) {
        window["logo"] = $(this).val();

        productAttributes["logo"] = getAttVal(window['logo']);
    });


    window['productId'] = $("#attributes").attr("data-productId");
    window['sku'] = $("#sku-" + window['productId']).text();

    //Ajax request for Invetory data
    $.ajax({
        type: "GET",
        url: "/api/InventoryWebService/GetInventoryDataByParentSku?parentSku=" + $("p#" + $(".product").children("p").attr("id")).first().text(),
        dataType: "json",
        success: function (data) {
            if (data.length == 0) {
                AllowAddToBasket();
            }
            else {


                for (var x = 0; x < data.length; x++) {
                    window['attributes'].push(data[x]);

                    if ($("a[name=colour]").length > 0) {
                        selectColour($("a[name=colour]").first());
                    }
                    else {

                        if ($("a[name=size]").length == 0) {
                            GetInventoryStatus(productAttributes);
                        }
                    }



                }
            }
        }
    });

    var elements = $(".attributes").map(function () {
        return this;
    });

    for (var x = 0; x < elements.length; x++) {
        var attributesElement = $(elements[x]).find("ul");

        for (i = 0; i < attributesElement.length; i++) {
            var elementId = $(attributesElement[i]).attr("id");
            $(attributesElement[i]).attr("id", elementId + "-" + $(elements[x]).attr("data-productid"));

        }

    }



});

//Functions

function RemoveHighlight(element) {
    $(element).parent().siblings().each(function () {
        $(this).find("a").removeClass("selectedAttribute");
    });
}

function RemoveAllHighlights(element) {
    $(element).parents("#attributes").find("a").removeClass("selectedAttribute");
    $(element).parents("#attributes").find("li").removeClass("selectedAttribute");
}

function GetSizes(arr, item) {
    for (var x = 0; x < arr.length; x++) {
        if (arr[x].colour == item) {
            window['sizes'].push(arr[x].size)
        }
    }
}
function GetColours(arr, item) {
    for (var x = 0; x < arr.length; x++) {
        if (arr[x].size == item) {
            window['colours'].push(arr[x].colour)
        }
    }
}

function AddSizeOpacity(arr) {
    $("ul#flxSize-" + window['productId']).children("li").addClass("outOfStock").each(function () {
        for (var x = 0; x < arr.length; x++) {
            if (arr[x] == $(this).attr("id")) {
                $(this).removeClass("outOfStock");
            }

        }
    });

    window['sizes'] = [];
}

function AddColourOpacity(arr) {
    $("ul#flxColour-" + window['productId']).children("li").addClass("outOfStock").each(function () {
        for (var x = 0; x < arr.length; x++) {
            if (arr[x] == $(this).attr("id")) {
                $(this).removeClass("outOfStock");
            }

        }
    });

    window['colours'] = [];
}


function getAttVal(attVal) {
    if (attVal == null) {
        window['isValid'] = false;
        return '';
    }
    else {
        return attVal;
    }
    return
}

function GetInventoryStatus(productAttributes) {

    var sku = $("#sku-" + window['productId']).text();
    var json = JSON.stringify(productAttributes);
    var data = "{'sku': '" + sku + "', 'attributes': '" + json + "'}";
    window['count'] = 1;


    $.ajax({
        type: "POST",
        url: "/api/InventoryWebService/GetInventoryStatusBySku",
        data: data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            var resp = JSON.parse(msg);
            // Replace the div's content with the page method's return.
            $("#stockStatus-" + window['productId']).text(resp);
            if (resp == "in stock") {
                AllowAddToBasket()

                $("#stockStatus-" + window['productId']).addClass("inStockText")
                $("#stockStatus-" + window['productId']).removeClass("outStockText")
            }
            else {
                PreventAddToBasket()

                $("#stockStatus-" + window['productId']).addClass("outStockText")
                $("#stockStatus-" + window['productId']).removeClass("inStockText")
            }
        },
        failure: function (msg) {
            alert("fail!");
        }
    });

    //Set parent window object properties
    window['selectedColour'] = window['colour'];
    window['selectedSize'] = window['size'];
    window['productId'] = window['productId'];
    window['sku'] = $("#sku-" + window['productId']).text();

};

function AllowAddToBasket() {
    window['allowAddToBasket'] = true;
    $(".addToBasket").removeClass("buttonDisable");
    $(".buy").removeClass("buttonDisable");
    $(".addToBasket").addClass("item-add-btn");
    $(".buy").addClass("item-add-btn");
}

function PreventAddToBasket() {

    window['allowAddToBasket'] = false;
    $(".addToBasket").addClass("buttonDisable");
    $(".buy").addClass("buttonDisable");
    $(".addToBasket").removeClass("item-add-btn");
    $(".buy").removeClass("item-add-btn");


}
