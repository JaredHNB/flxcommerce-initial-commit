﻿//Global Vars
var _qty = 0;

flxCommerceApp.directive('qtyUpdate', function ($http, flxOrdersResource, $timeout) {
    return {
        restrict: 'EA',
        require: 'ngModel',
        link: function ($scope, elm, attr, ngModelCtrl) {
            if (attr.type === 'radio' || attr.type === 'checkbox') return;
            //elm.unbind('input').unbind('keydown').unbind('change');
            elm.bind('change', function () {
                $timeout(updateOrderLineQuantity, 2500);
            });
            function updateOrderLineQuantity() {
                if (_qty != elm.val()) {
                    _qty = elm.val();
                    $scope.$apply(function () {
                        var data = { orderLineId: $scope.orderline.OrderLineID, quantity: elm.val(), executeWorkflow: 'basket' };
                        flxOrdersResource.updateOrderLine(data).then(function () {
                            GetCurrentOrder();
                        });
                    });
                }
            };
            function GetCurrentOrder() {
                flxOrdersResource.getCurrentCustomerOrder().then(function (response) {
                    $scope.$parent.Order = response.data;
                })
            };

        }
    };
});