﻿flxCommerceApp.directive('addOrderLine', function ($http) {
    return {
        restrict: 'AEC',
        compile: function (tElem, tAttrs) {
            return {
                post: function ($scope, iElem, iAttrs, controller) {

                    //On click event
                    iElem.bind('click', function (e) {
                        if (!$scope.buttonDisable) {
                            $scope.$emit('addOrderLine', $scope.Product);
                        }
                    });
                }
            }
        },

        controller: function ($scope) {


            $scope.buttonDisable = true;

            $scope.$on('enableButton-addOrderLine', function (e) {
                $scope.buttonDisable = false;
                $scope.$digest();
            });
            $scope.$on('disableButton-addOrderLine', function (e) {
                $scope.buttonDisable = true;
                $scope.$digest();

            });
        },
    };
}); flxCommerceApp.directive('addOrderLineRedirect', function ($http) {
    return {
        restrict: 'AEC',
        compile: function (tElem, tAttrs) {
            return {
                post: function ($scope, iElem, iAttrs, controller) {

                    //On click event
                    iElem.bind('click', function (e) {
                        if (!$scope.buttonDisable) {
                            $scope.$emit('addOrderLineRedirect', '/Basket', $scope.Product);
                        }
                    });
                }
            }
        },

        controller: function ($scope) {


            $scope.buttonDisable = true;

            $scope.$on('enableButton-addOrderLineRedirect', function (e) {
                $scope.buttonDisable = false;
                $scope.$digest();
            });
            $scope.$on('disableButton-addOrderLineRedirect', function (e) {
                $scope.buttonDisable = true;
                $scope.$digest();

            });
        },
    };
});