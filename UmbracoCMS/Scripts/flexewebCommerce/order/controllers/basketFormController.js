//Global Vars
var _qty = 0;

flxCommerceApp.config(function ($httpProvider) {
    $httpProvider.defaults.headers.get = {};
    $httpProvider.defaults.headers.get["Content-Type"] = "application/json; charset=utf-8";
});


flxCommerceApp.directive('ngModelOnblur', function ($http, $timeout) {
    return {
        restrict: 'EA',
        require: 'ngModel',
        link: function ($scope, elm, attr, ngModelCtrl) {
            if (attr.type === 'radio' || attr.type === 'checkbox') return;
            //elm.unbind('input').unbind('keydown').unbind('change');
            elm.bind('change', function () {
                $timeout(updateOrderLineQuantity, 1500);
            });
            function updateOrderLineQuantity() {
                if (_qty != elm.val()) {
                    _qty = elm.val();
                    $scope.$apply(function () {
                        $http({
                            method: 'POST',
                            url: '/api/OrderWebService/UpdateOrderLineQuantity',
                            data: 'orderLineId=' + $scope.product.OrderLineID + '&quantity=' + elm.val() + '&executeWorkflow=basket',
                            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                        }).success(function (data, status, headers, config) {
                            if (window.console && console.log) {
                                console.log(data);
                            }
                            bindbasket($scope, $http);
                            $scope.$parent.showSuccessAlert = true;
                            // switch flag
                            $scope.$parent.switchBool = function (value) {
                                $scope.$parent[value] = !$scope.$parent[value];
                            };
                        })
                    });
                }
            }
            function bindbasket($scope, $http) {
                $http({ method: "Get", url: "/api/OrderWebService/GetCurrentCustomerOrder" })
                .success(function (data) {
                    if (window.console && console.log) {
                        console.log(data);
                    }
                    console.log("binding basket");
                    $scope.$parent.order = data;

                });
            }

        }
    };
});

flxCommerceApp.filter('addthumbnailsuffix', function () {
    return function (text) {
        return text.replace(".jpg", "_thumb_170.jpg");
    };
})
flxCommerceApp.filter('removechars', function () {
    return function (text, chars) {
        for (var i = 0; i < chars.length; i++) {
            text = text.replace(chars[i], "");
        }
        return text;
    };
})


flxCommerceApp.controller('BasketForm', function ($scope, $http, $filter) {

    bindbasket($scope, $http);
    getCountries($scope, $http);
    //set your default shipping country here
    $scope.shippingcountry = "GB";
    $scope.message = "";
    //List of countries
    $scope.dataC = {
        countryList: []
    };
    
    


    $scope.shippingOptions = {
        show: false,
        data: null,
    };

    $scope.couponSuccess = {
        status: false,
        message: "Your discount has been applied"
    }

    $scope.couponFailure = {
        status: false,
        message: "Sorry, your discount code did not get applied. Please contact 01656 646404 for assistance."
    }

    $scope.ShowCoupon = false;

    $scope.removeItem = function (olId) {
        $http({
            method: 'POST',
            url: '/api/OrderWebService/DeleteOrderLine',
            data: 'orderLineId=' + olId + '&executeWorkflow=basket',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).success(function (data, status, headers, config) {
            if (window.console && console.log) {
                console.log(data);
            }
            bindbasket($scope, $http);
            $scope.showSuccessAlert = true;
            // switch flag
            $scope.switchBool = function (value) {
                $scope[value] = !$scope[value];
            };
        });
    }

    function bindbasket($scope, $http) {
        $http({ method: "Get", url: "/api/OrderWebService/GetCurrentCustomerOrder" })
        .success(function (data) {
            if (window.console && console.log) {
                console.log(data);
            }
            if (data.OrderLines.length == 0) {
                $scope.message = "is currently empty.";
            }
            $scope.order = data;
            hasCouponCode($scope.order);
            $scope.CouponCode = $scope.order.CouponCode;

        });
    }

    //Gets all the countries from the Location API
    function getCountries($scope, $http) {
        $http({ method: "Get", url: "/api/LocationWebService/GetAllCountries" })
        .success(function (data) {
            $scope.dataC.countryList = data;           
        });
    }

    $scope.assignCoupon = function () {
        $http({
            method: 'POST',
            url: '/api/OrderWebService/AssignCouponCode',
            data: 'couponCode=' + $scope.couponCode + '&executeWorkflow=' + 'basket',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).success(function (data, status, headers, config) {
            if (window.console && console.log) {
                bindbasket($scope, $http);
            }
        }).then(function () {
            $scope.$watch("CouponCode", function (newVal, oldVal) {
                isSuccess($scope.order);
            });
        })
    }

    function hasCouponCode(order) {
        if (order.CouponCode != null) {
            $scope.ShowCoupon = true;
        }
        else {
            $scope.ShowCoupon = false;
        }
    }


    function isSuccess(order) {
        if (order.CouponCode != null) {
            $scope.couponSuccess.status = true;
            $scope.couponFailure.status = false;

        }
        else {
            $scope.couponFailure.status = true;
            $scope.couponSuccess.status = false;

        }
    }

    $scope.getQuotes = function () {
        $http({
            method: 'POST',
            url: '/api/FulfillmentWebService/GetShippingOptions',
            data: "{'countryCode': '" + $scope.shippingcountry + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
        }).success(function (data, status, headers, config) {
            //makes div visible
            var shippingoptionlist = document.getElementById("shippingoptionlist");
            //sets scope variable to data returned from API
            if (data.length == 0) {
                $scope.shippingOptions.message = "There are no shipping options to your destination. Please contact support on  +44 (0) 161 368 0113 for a personalised quote";
                $scope.shippingOptions.data = null;
                $scope.shippingOptions.show = true;
            } else {
                $scope.shippingOptions.message = null;
                $scope.shippingOptions.data = data;
                $scope.shippingOptions.show = true;
            }

        });

    }

    $scope.SetShippingOptions = function (option) {
        $http({
            method: 'POST',
            url: '/api/OrderWebService/SetFulfillmentOption',
            data: { shippingOption: option, executeWorkflow: 'basket' },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
        }).success(function (data, status, headers, config) {
            bindbasket($scope, $http);

        });
    }

});