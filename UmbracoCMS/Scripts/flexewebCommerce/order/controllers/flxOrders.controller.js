﻿flxCommerceApp.filter('addthumbnailsuffixmb', function () {
    return function (text) {
        return text.replace(".jpg", "_thumb_90.jpg");
    };
})


flxCommerceApp.controller("flxOrders.Controller",
   function ($scope, flxOrdersResource, $timeout, $http) {

       $scope.ShowOrder = false;
      $scope.Quantity = 1;

       function GetCurrentOrder() {
           flxOrdersResource.getCurrentCustomerOrder().then(function (response) {
               $scope.Order = response.data;
           });
       };

       function AddOrderLineInternal(product) {


           var qty = 1;

           if (isNaN()) {
               qty = parseInt($scope.Quantity);
           }

           if (!isNaN(qty) && qty != 0) {

               $scope.$apply(function () {

                   var providerArguments = new Object;

                   providerArguments["sku"] = product.Sku;
                   providerArguments["quantity"] = qty;
                   providerArguments["attributes"] = product.SelectedAttributes;
                   providerArguments["options"] = product.SelectedOptionCollection;
                   providerArguments["executeWorkflow"] = "basket";

                   flxOrdersResource.addOrderline(providerArguments).then(function (response) {
                       GetCurrentOrder();
                   });

               });

               return true;
           }
           else {
               alert("Please make sure your quantity is a numerical value e.g 1, 2 and is NOT 0")
               return false;
           }

       }

       $scope.$on('addOrderLine', function (e, product) {

           if (AddOrderLineInternal(product)) {              
               $("#addToBasketMessage")[0].innerText = "Course added to basket";
               $(".close", window.parent.document).trigger("click");
               $("html, body").animate({ scrollTop: 0 }, "slow");
               $(".dropdown-menu").addClass("show-DropDown");

               setTimeout(function () {
                   $(".dropdown-menu").removeClass("show-DropDown");
               }, 4000);

           }
       });


       $scope.AddOrderline = function AddOrderline(product) {

           if (AddOrderLineInternal(product)) {

               $(".close", window.parent.document).trigger("click");
               $("html, body").animate({ scrollTop: 0 }, "slow");
               $(".dropdown-menu").addClass("show-DropDown");
               setTimeout(function () {
                   $(".dropdown-menu").removeClass("show-DropDown");
               }, 4000);

           }

       }

       $scope.$on('addOrderLineRedirect', function AddOrderlineAndRedirect(e, redirectUrl, product) {
           if (AddOrderLineInternal(product)) {
               window.location = redirectUrl;
           }
       });

       $scope.AddOrderlineAndRedirect = function AddOrderlineAndRedirect(redirectUrl, product) {
           if (AddOrderLineInternal(product)) {
               window.location = redirectUrl;
           }
       }

       $scope.deleteOrderLine = function (orderId) {
           var data = { orderLineId: orderId, executeWorkflow: 'basket' };
           flxOrdersResource.deleteOrderLine(data).then(function () {
               GetCurrentOrder();
           });
       };

       GetCurrentOrder();

       //Broadcast to submit button that the process is complete 
       $scope.$on('enableButton', function (e, button) {
           switch (button) {
               case 'addOrderLine': $timeout(function () { $scope.$broadcast('enableButton-addOrderLine'); });
               case 'addOrderLineRedirect': $timeout(function () { $scope.$broadcast('enableButton-addOrderLineRedirect'); });
           }
       });
       //Broadcast to submit button that the process is complete 
       $scope.$on('disableButton', function (e, button) {
           switch (button) {
               case 'addOrderLine': $timeout(function () { $scope.$broadcast('disableButton-addOrderLine') });
               case 'addOrderLineRedirect': $timeout(function () { $scope.$broadcast('disableButton-addOrderLineRedirect') });
           }
       });

       $scope.formData = {};
       $scope.formData.Name = '';
       $scope.formData.List = 'default';
       $scope.subscribeMessage = "";
       $scope.processForm = function () {
           $http({
               method: 'POST',
               url: '/api/NewsletterWebService/AddSubscriber',
               data: $scope.formData,
               contentType: "application/json; charset=utf-8",
               dataType: "json",
           }).success(function (data, status, headers, config) {

               $scope.subscribeMessage = "Thank you for subcribing!";

           });
       };

       $scope.removeItem = function (olId) {
           $http({
               method: 'POST',
               url: '/api/OrderWebService/DeleteOrderLine',
               data: 'orderLineId=' + olId + '&executeWorkflow=basket',
               headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
           }).success(function (data, status, headers, config) {
               if (window.console && console.log) {
                   console.log(data);
               }
               bindbasket($scope, $http);

           });
       };

       function bindbasket($scope, $http) {
           $http({ method: "Get", url: "/api/OrderWebService/GetCurrentCustomerOrder" })
               .success(function (data) {
                   if (window.console && console.log) {
                       console.log(data);
                   }
                   if (data.OrderLines.length == 0) {
                       $scope.message = "is currently empty.";
                   }
                   $scope.Order = data;
                   hasCouponCode($scope.Order);
                   $scope.CouponCode = $scope.Order.CouponCode;

               });
       };

       function hasCouponCode(order) {
           if (order.CouponCode != null) {
               $scope.ShowCoupon = true;
           }
           else {
               $scope.ShowCoupon = false;
           }
       };


   });

