﻿flxCommerceApp.controller('PaymentCtrl', function ($scope, $http, $window) {

    // *****Scope variables*****
    $scope.currentOrder = null;
    $scope.myPointerEvents = 'none';
    $scope.myOpacity = '0.5';
    $scope.shippingOptions = {};
    $scope.selectedOption = null;
    $scope.updateButton = false;


    // *****AJAX methods*****
    // Gets the order currently being used
    $scope.GetCurrentOrder = function () {
        $http({
            method: 'GET',
            url: '/api/orderwebservice/getcurrentcustomerorder'
        }).success(function (data) {
            $scope.currentOrder = data;
            $scope.myPointerEvents = 'auto';
            $scope.myOpacity = '1';
            getQuotes();

        });
    };

    function getQuotes() {
        $http({
            method: 'POST',
            url: '/api/FulfillmentWebService/GetShippingOptions',
            data: "{'countryCode': '" + $scope.currentOrder.ShippingAddress.Country + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
        }).success(function (data, status, headers, config) {

            $scope.shippingOptions = data;
            //match the selected element with the one in the order so the form is initialised correctly
            var elementPos = $scope.shippingOptions.map(function (x) { return x.FulfillmentOptionID; }).indexOf($scope.currentOrder.FulfillmentOption.FulfillmentOptionID);
            $scope.selectedOption = $scope.shippingOptions[elementPos];

        });

    }
    $scope.disablePayment = function () {
        $scope.myPointerEvents = 'none';
        $scope.myOpacity = '0.5';
        $scope.updateButton = true;
    }

    // Updates the current order
    $scope.SetFulfillmentOption = function (fulfillmentOptionID, shipping) {
        var option = {};
        option.FulfillmentOptionID = fulfillmentOptionID;
        option.Rate = shipping;
        $scope.myPointerEvents = 'none';
        $scope.myOpacity = '0.5';
        $http({
            method: 'POST',
            url: '/api/orderwebservice/setfulfillmentoption',
            data: { shippingOption: option, executeWorkflow: 'basket' },
        }).success(function () {
            $window.location = "./?v=" + Math.floor((Math.random() * 100) + 1);

        });
    };

    $scope.WorldPayPayment = function (token) {


        $http({
            method: 'POST',
            url: '/api/PaymentGatewayWebService/RegisterTransactionWithWorldPay',
            data: token,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
        }).success(function (data, status, headers, config) {


        });
    }


    // *****Initialize*****
    $scope.GetCurrentOrder();
});