﻿flxCommerceApp.controller("flxChildOrders.Controller",
   function ($scope, flxOrdersResource, $parentScope) {

       $scope.ParentOrder = $parentScope.getParentOrderScope();
       $scope.Quantity = 1;

       $scope.$watch('Quantity', function (newValue, oldValue) {
           $scope.ParentOrder.Quantity = $scope.Quantity;
       });

       $scope.$on('addOrderLine', function (e, product) {
           
           $scope.ParentOrder.AddOrderline(product);
           $scope.ParentOrder.$apply();
       });

       //Broadcast to submit button that the process is complete 
       $scope.$on('enableButton', function (e, button) {
           switch (button) {
               case 'addOrderLine': $scope.$broadcast('enableButton-addOrderLine');
           }
       });
       //Broadcast to submit button that the process is complete 
       $scope.$on('disableButton', function (e, button) {
           switch (button) {
               case 'addOrderLine': $scope.$broadcast('disableButton-addOrderLine');
           }
       });



   });



