﻿flxCommerceApp.service("simpleInventoryService", function ($filter) {


    this.getInventoryDataByParentSku = function (parentSku) {
        return null;
    };

    this.isInStock = function (data) {

        if (data.Type != data.SelectedAttrType) {
            return isInStock(data.AttributeBundle, data.Value, $filter);
        }
        else {
            return true;
        }

    }
});

function isInStock(attributes, selectedAttribute, $filter) {

    var attrs = $filter('filter')(attributes, selectedAttribute, true);

    if (attrs.length == 0) {
        return false;
    }
    else {
        return true;
    }
    
}

