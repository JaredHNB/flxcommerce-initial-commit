﻿flxCommerceApp.directive('attrStandard', function ($http) {
    return {
        restrict: 'AEC',
        require: '^attrParent',
        scope: [],
        compile: function (tElem, tAttrs) {
            window['flxAttributeTypesCollection'].push(tAttrs.name);
            return {
                post: function ($scope, iElem, iAttrs, controller) {
                    //Initilase event
                    $scope.Type = iAttrs.name;
                    $scope.Value = iAttrs.id;

                    //On click event
                    iElem.bind('click', function (e) {
                        //$scope.$apply(function () {
                        //});
                        $scope.isSelected = true;
                        $scope.$digest();

                        controller.callBroadCast($scope.Value, $scope.Type, $scope.isInStock);
                    });
                }
            }
        },

        controller: function ($scope) {

            //Properties
            $scope.isInStock = true;
            $scope.isSelected = false;
            $scope.Type = null;
            $scope.Value = null;

            $scope.$on('getInventoryService', function (e, data, inventoryService) {

                data.Value = $scope.Value;
                data.Type = $scope.Type;
                $scope.isInStock = inventoryService.isInStock(data);
                $scope.$digest();

            });


            $scope.$on('setIsSelectedToFalse', function (e, selectedAttrValue) {

                if (selectedAttrValue != $scope.Value) {
                    $scope.isSelected = false;
                    $scope.$digest();

                }
            });

        },
    };
});
