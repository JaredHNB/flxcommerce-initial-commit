﻿//Global vars
window['flxAttributeTypesCollection'] = [];
window['flxAttributeTypeGroupBy'] = [];

flxCommerceApp.directive('attrParent', function ($http, simpleInventoryService, flxInventoryResource, attributesProcessManagementService, $filter) {
    return {
        restrict: 'AEC',
        compile: function (tElem, tAttrs) {
            return {
                pre: function ($scope, iElem, iAttrs, controller) {
                    $scope.loading = false;
                },
                post: function ($scope, iElem, iAttrs, controller) {
                    init($scope, iElem);
                },
            }
        },
        controller: function ($scope, simpleInventoryService, flxInventoryResource, attributesProcessManagementService, $filter, $timeout) {

            var _inventoryBundle = [];
            var _count = 0;
            var _selectedAttributesDict = [];

            this.callBroadCast = function (selectedAttrValue, attrType, isInStock) {

                if (window['flxAttributeTypeGroupBy'].length > 1) {

                    //Process management data must be key value pair
                    var processData = {
                        Key: 'AttrData',
                        Value: { SelectedAttrType: attrType, isInStock: isInStock }
                    };


                    if (isInStock) {
                        //Call Process management service
                        attributesProcessManagementService.updateProcessState(processData);

                        var action = attributesProcessManagementService.getCurrentAction(processData);

                        if (action == 'start') {

                            //delete existing selected attributes data
                            _selectedAttributesDict = [];

                            //Add new selected attribute data
                            _selectedAttributesDict[attrType] = selectedAttrValue;

                            start($scope, selectedAttrValue, attrType);
                        }
                        if (action == 'restart') {

                            //delete existing selected attributes data
                            _selectedAttributesDict = [];

                            //Add new selected attribute data
                            _selectedAttributesDict[attrType] = selectedAttrValue;

                            restart($scope, selectedAttrValue, _selectedAttributesDict);
                            start($scope, selectedAttrValue, attrType, _selectedAttributesDict);
                        }
                        if (action == 'complete') {

                            //Add new selected attribute data
                            _selectedAttributesDict[attrType] = selectedAttrValue;

                            complete($scope, _selectedAttributesDict);
                        }
                    }
                    else {

                        //Call Process management service
                        attributesProcessManagementService.updateProcessState(processData);

                        var action = attributesProcessManagementService.getCurrentAction(processData);

                        if (action == 'restart') {

                            //delete existing selected attributes data
                            _selectedAttributesDict = [];

                            //Add new selected attribute data
                            _selectedAttributesDict[attrType] = selectedAttrValue;

                            restart($scope, selectedAttrValue, _selectedAttributesDict);
                            start($scope, selectedAttrValue, attrType);

                        };
                    }
                };
                if (window['flxAttributeTypeGroupBy'].length == 1) {

                }

            };
        },
    };

    //Private function
    function init($scope, iElem) {

        for (var i = 0; i < window['flxAttributeTypesCollection'].length; i++) {
            var type = window['flxAttributeTypesCollection'][i];

            if ($filter('filter')(window['flxAttributeTypeGroupBy'], type, true).length == 0) {
                window['flxAttributeTypeGroupBy'].push(type);
            }
        }

        if (window['flxAttributeTypeGroupBy'].length != 0) {

            var data = {
                Key: 'AttrTypes',
                Value: window['flxAttributeTypeGroupBy'].length
            }

            //Initialise process management
            attributesProcessManagementService.initialise(data);

            //Call Inventory api
            var parentSku = $scope.Product.Sku;
            flxInventoryResource.getInventoryDataByParentSku(parentSku).then(function (response) {
                if (response.data.length > 0) {
                    $scope.inventoryData = response.data;

                    if (window['flxAttributeTypeGroupBy'].length == 1) {
                        preInit($scope);
                    };

                    $scope.loading = true;

                };

            });
        };
    }

    function preInit($scope)
    {

        var inventoryCheckData = {
            Value: null,
            Type: null,
            SelectedAttrType: attrType,
            SelectedAttrVal: selectedAttrValue,
            AttributeBundle: $scope.inventoryData
        };


        //Broadcast to every directive and pass the simpleInventoryService to each directive 
        $scope.$broadcast('getInventoryService', inventoryCheckData, simpleInventoryService);

    }

    function start($scope, selectedAttrValue, attrType)
    {
        _inventoryBundle = $filter('filter')($scope.inventoryData, selectedAttrValue, true);

        var inventoryCheckData = {
            Value: null,
            Type: null,
            SelectedAttrType: attrType,
            SelectedAttrVal: selectedAttrValue,
            AttributeBundle: _inventoryBundle
        };


        //Broadcast to every directive and pass the simpleInventoryService to each directive 
        $scope.$broadcast('getInventoryService', inventoryCheckData, simpleInventoryService);

    }

    function restart($scope, selectedAttrValue, _selectedAttributesDict) {

        $scope.$broadcast('setIsSelectedToFalse', selectedAttrValue);
        $scope.$broadcast('disableButton-addOrderLine');
    }

    function complete($scope, _selectedAttributesDict)
    {
        //Broadcast to submit button that the process is complete 
        $scope.$broadcast('enableButton-addOrderLine', _selectedAttributesDict);

    }

});

