﻿//Global Variables
var attributes = [];

flxCommerceApp.provider("simpleInventoryProvider", function ($injector) {

    var _inventoryResource = $injector.get("flxInventoryResource")

    this.isInStock = function (data) {

        if (attributes.length == 0) {
            var allAttributes = GetAttributes(_inventoryResource, $("p#" + $(".product").children("p").attr("id")).first().text());

            attributes = FilterAttributes(allAttributes, data.Value, $filter);
        }

        if(data.Type != data.SelectedType) {
            return isInStock(attributes, data.Value, $filter);
        }
    }

    this.$get = [function () {
        return this;
    }];

});

function GetAttributes(flxInventoryResource, parentSku) {

    flxInventoryResource.getInventoryDataByParentSku(parentSku).then(function (response) {
        if (response.data.length > 0) {
            return response.data;
        };

    });

}
function FilterAttributes(allAttributes, selectedAttribute, $filter) {

    var attributes = $filter('filter')(allAttributes, selectedAttribute, true);

    return attributes;
    
}
function isInStock(attributes, selectedAttribute, $filter) {

    var attributes = $filter('filter')(allAttributes, selectedAttribute, true);

    if(attributes == null && attributes.length == 0) {
        return false;
    }
    else {
        return true;
    }
    
}

