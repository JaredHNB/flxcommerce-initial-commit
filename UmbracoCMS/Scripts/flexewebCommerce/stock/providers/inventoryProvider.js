﻿flxCommerceApp.provider("inventoryProvider", [function () {

    var provider = null;

    this.setProvider = function (providerArg) {
        provider = providerArg;
    };

    this.$get = [function () {
        return provider;
    }];
}]);