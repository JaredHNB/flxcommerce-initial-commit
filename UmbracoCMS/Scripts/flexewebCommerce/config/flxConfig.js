﻿flxCommerceApp.config(["inventoryProviderProvider", "simpleInventoryProviderProvider", function (inventoryProviderProvider, simpleInventoryProviderProvider) {
    inventoryProviderProvider.setProvider(simpleInventoryProviderProvider);
}]);