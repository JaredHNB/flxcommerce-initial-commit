﻿flxCommerceApp.controller('flxProductController', function ($scope, $element, $filter, $timeout, flxCatalogueResource) {

    //Change name to Loading Attributes 
    $scope.loading = true;
    $scope.LoadingOptions = true;

    //Set Product Properties
    $scope.Product = { Id: null, Sku: null, SelectedAttributes: [], Options: null, SelectedOptionCollection: [] };
    $scope.Product.Id = $element[0].id.split('-')[1];
    // var productElement = angular.element('.product');
    $scope.Product.Sku = document.getElementById('sku').textContent;
    flxCatalogueResource.getOptionsByProductId($scope.Product.Id).then(function (response) {
        $scope.Product.Options = response.data;

        //Filter Options types into different option scopes
        $scope.PersonalisationOptions = $filter('filter')($scope.Product.Options, { OptionType: "Personalisation" });
        $scope.GiftWrap = $filter('filter')($scope.Product.Options, { OptionType: "GiftWrap" });

        $timeout(function () { $scope.$broadcast('Options-Personalisation', $scope.PersonalisationOptions) });
        $timeout(function () { $scope.$broadcast('Options-GiftWrap', $scope.GiftWrap) });
    });

});