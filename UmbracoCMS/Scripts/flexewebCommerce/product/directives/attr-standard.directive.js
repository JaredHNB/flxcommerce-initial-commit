﻿flxCommerceApp.directive('attrStandard', function ($http, $timeout) {
    return {
        restrict: 'AEC',
        require: '^attrParent',
        scope: [],
        compile: function (tElem, tAttrs) {
            window['flxAttributeDataCollection'].push({ AttrType: tAttrs.name, AttrValue: tAttrs.id, AttrElem: tElem[0] });
            return {
                post: function ($scope, iElem, iAttrs, controller) {
                    //Initilase event
                    $scope.CurrentAttrType = iAttrs.name;
                    $scope.CurrentAttrValue = iAttrs.id;

                    //On click event
                    iElem.bind('click', function (e) {
                        //$scope.$apply(function () {
                        //});
                        if ($scope.isInStock) {
                            iElem.addClass('selectedAttribute');
                            $scope.isSelected = true;
                            var quantityNode = this.getAttribute('flx-data-id');
                            var stockLevel = this.getAttribute('flx-data-stock');
                            var quantityN = document.getElementById("quantity-" + quantityNode);
                            quantityN.max = stockLevel;
                            if (parseInt(quantityN.value) > parseInt(stockLevel))
                            {
                                quantityN.value = stockLevel;
                            }

                            //Ensure $digest is not called if one is in process
                            $timeout(function () {
                                $scope.$digest();
                            })
                            controller.callBroadCast($scope.CurrentAttrValue, $scope.CurrentAttrType, $scope.isInStock);
                        }
                    });

                    $scope.$on('calculateStock', function (e, data, inventoryService) {

                        data.CurrentAttrValue = $scope.CurrentAttrValue;
                        data.CurrentAttrType = $scope.CurrentAttrType;

                        if (data.AttrTypeCount == 1) {
                            $scope.isInStock = inventoryService.isInStock(data);
                           // $scope.stockLevel = inventoryService.getInventoryDataByParentSku(data);
                        }
                        else {
                            if (data.CurrentAttrType != data.InitAttrType) {
                                if (data.CurrentAttrType != data.SelectedAttrType) {
                                    $scope.isInStock = inventoryService.isInStock(data);
                                }
                            }
                        }

                        if (!$scope.isInStock) {
                            iElem.addClass('outOfStock');
                           
                        } else {
                            iElem.removeClass('outOfStock');
                           // var newElement = document.createElement('span');
                            //newElement.innerText = " Spaces Left : " + $scope.stockLevel;
                            //var elementParent = iElem[0].parentNode;
                            //elementParent.before(newElement, iElem[0].nextSibling);
                         
                            //console.log(iElem[0].labels[0].textContent += " Spaces : " + $scope.stockLevel);
                        }

                    });
                    $scope.$on('setIsSelectedToFalse-AllTypes', function (e, attrType, selectedAttrValue) {

                        if (selectedAttrValue != $scope.CurrentAttrValue) {
                            $scope.isSelected = false;
                            iElem.removeClass('selectedAttribute');

                            //Ensure $digest is not called if one is in process
                            $timeout(function () {
                                $scope.$digest();
                            })
                        }
                    });
                    $scope.$on('setIsSelectedToFalse-SingleType', function (e, attrType, selectedAttrValue) {

                        if (attrType == $scope.CurrentAttrType) {
                            if (selectedAttrValue != $scope.CurrentAttrValue) {
                                $scope.isSelected = false;
                                iElem.removeClass('selectedAttribute');

                                //Ensure $digest is not called if one is in process
                                $timeout(function () {
                                    $scope.$digest();
                                })

                            }
                        }
                    });
                    $scope.$on('getSelectedValue', function (e) {

                        if ($scope.isSelected) {
                            window['flxSelectedAttribute'][$scope.CurrentAttrType] = $scope.CurrentAttrValue;
                        }

                    });

                }
            }
        },

        controller: function ($scope) {

            //Properties
            $scope.isInStock = true;
            $scope.isSelected = false;
            $scope.CurrentAttrValue = null;
            $scope.CurrentAttrType = null;
            //$scope.stockLevel = 0;

        },
    };
});
