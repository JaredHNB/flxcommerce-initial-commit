﻿//Global vars
window['flxAttributeDataCollection'] = [];
window['flxAttributeTypeCount'] = [];
window['flxAttributeBundle'] = [];

flxCommerceApp.directive('attrParent', function ($http, simpleInventoryService, flxInventoryResource, attributesProcessManagementService, $filter, $timeout) {
    return {
        restrict: 'AEC',
        compile: function (tElem, tAttrs) {
            return {
                pre: function ($scope, iElem, iAttrs, controller) {
                    $scope.outOfStockMessage = false;

                },
                post: function ($scope, iElem, iAttrs, controller) {
                    init($scope, iElem);
                },
            }
        },
        controller: function ($scope, simpleInventoryService, flxInventoryResource, attributesProcessManagementService, $filter, $timeout) {

            var _selectedAttributesDict = new Object;
            var _attrType = null;
            var _stockStatusCount = 0;

            this.callBroadCast = function (selectedAttrValue, attrType, isInStock) {

                    //Remove select higlight from only a single attribute type 
                    $scope.$broadcast('setIsSelectedToFalse-SingleType', attrType, selectedAttrValue);

                    if(window['flxAttributeTypeCount'].length != 1) {
                        if (isInStock) {

                            //Ensure $digest is not called if one is in process
                            $timeout(function () {
                                //Update scope on Out of stock message
                                $scope.$digest();
                            })


                            //*Use Process management service to help keep track of the select attribute process*//

                            //Process management data must be key value pair
                            var processData = {
                                Key: 'AttrData',
                                Value: { SelectedAttrType: attrType, isInStock: isInStock }
                            };

                            //Call Process management service
                            attributesProcessManagementService.updateProcessState(processData);

                            var action = attributesProcessManagementService.getCurrentAction(processData);

                            if (action == 'start') {

                                start($scope, attrType, selectedAttrValue);
                            }
                            if (action == 'resume') {
                                resume($scope, attrType, selectedAttrValue);

                            }
                            if (action == 'restart') {

                                restart($scope, attrType, selectedAttrValue);
                                start($scope, attrType, selectedAttrValue);
                            }
                            if (action == 'complete') {
                                complete($scope);
                            }
                        }
                    }
                    else {
                        if (isInStock) {
                            complete($scope);
                        }
                    }
            };

        },
    };

    //Private function
    function init($scope, iElem) {

        //Calculate the no. of different attribute types
        for (var i = 0; i < window['flxAttributeDataCollection'].length; i++) {
            var type = window['flxAttributeDataCollection'][i].AttrType;

            if ($filter('filter')(window['flxAttributeTypeCount'], type, true).length == 0) {
                window['flxAttributeTypeCount'].push(type);
            }
        }

        if (window['flxAttributeTypeCount'].length != 0) {


            //Call Inventory api
            var parentSku = $scope.Product.Sku;
            flxInventoryResource.getInventoryDataByParentSku(parentSku).then(function (response) {
                if (response.data.length > 0) {
                    $scope.inventoryData = response.data;

                    postInit($scope, window['flxAttributeTypeCount'].length);
                };

            });
        }
        else {

            //Broadcast to Order controller button that the process is complete 
            $timeout(function () {
                $scope.$emit('enableButton', 'addOrderLine');
                $scope.loading = false;
            })

        }
    }

    function postInit($scope, attrTypeCount) {

        //Define behviour of child attributes if Attr Type Count is greater than 1
        if (attrTypeCount > 1) {

            var dataArr = []

            dataArr.push({ Key: 'AttrTypeCount', Value: window['flxAttributeTypeCount'].length });
            dataArr.push({ Key: 'InitAttrType', Value: window['flxAttributeDataCollection'][0].AttrType });

            //Initialise process management
            attributesProcessManagementService.initialise(dataArr);

            //Perform onclick event on first attribute
            angular.element(window['flxAttributeDataCollection'][0].AttrElem).trigger('click');

            $scope.loading = false;


        };

        //Define behviour of child attributes ifIf Attr Type Count is equals 1

        if (attrTypeCount == 1) {

            var attributeData = {
                CurrentAttrValue: null,
                CurrentAttrType: null,
                InitAttrType : window['flxAttributeDataCollection'][0].AttrType,
                AttrTypeCount: attrTypeCount,
                SelectedAttrType: null,
                SelectedAttrVal: null,
                AttributeBundle: $scope.inventoryData
            };


            //Broadcast to every directive and pass the simpleInventoryService to each directive 
            $scope.$broadcast('calculateStock', attributeData, simpleInventoryService);
            $scope.loading = false;

        }

        if (attrTypeCount == 0) {

            $scope.Product.SelectedAttributes = [];

            //Broadcast to submit button that the process is complete 
            $scope.$emit('enableButton', 'addOrderLine');
            $scope.$emit('enableButton', 'addOrderLineRedirect');

        }
    }

    function start($scope, attrType, selectedAttrValue) {
        window['flxAttributeBundle'] = $filter('filter')($scope.inventoryData, selectedAttrValue, true);

        var attributeData = {
            CurrentAttrValue: null,
            CurrentAttrType: null,
            InitAttrType: window['flxAttributeDataCollection'][0].AttrType,
            AttrTypeCount: window['flxAttributeTypeCount'].length,
            SelectedAttrType: attrType,
            SelectedAttrVal: selectedAttrValue,
            AttributeBundle: window['flxAttributeBundle']
        };


        //Broadcast to every directive and pass the simpleInventoryService to each directive 
        $scope.$broadcast('calculateStock', attributeData, simpleInventoryService);

    }

    function resume($scope, attrType, selectedAttrValue) {

        inventoryBundle = $filter('filter')(window['flxAttributeBundle'], selectedAttrValue, true);

        var attributeData = {
            CurrentAttrValue: null,
            CurrentAttrType: null,
            InitAttrType: window['flxAttributeDataCollection'][0].AttrType,
            AttrTypeCount: window['flxAttributeTypeCount'].length,
            SelectedAttrType: attrType,
            SelectedAttrVal: selectedAttrValue,
            AttributeBundle: inventoryBundle
        };


        //Broadcast to every directive and pass the simpleInventoryService to each directive 
        $timeout(function () {
            $scope.$broadcast('calculateStock', attributeData, simpleInventoryService);
        })

    }

    function restart($scope, attrType, selectedAttrValue) {
        
        $scope.$broadcast('setIsSelectedToFalse-AllTypes', attrType, selectedAttrValue);
        $scope.$emit('disableButton', 'addOrderLine');
        $scope.$emit('disableButton', 'addOrderLineRedirect');
    }

    function complete($scope) {

        window['flxSelectedAttribute'] = new Object;
        $scope.$broadcast('getSelectedValue');

        //Delete current attributes stored in Product scope 
        $scope.Product.SelectedAttributes = [];

        //Set attributes to Product scope
        $scope.Product.SelectedAttributes = window['flxSelectedAttribute'];

        //Broadcast to submit button that the process is complete 
        $scope.$emit('enableButton', 'addOrderLine');
        $scope.$emit('enableButton', 'addOrderLineRedirect');
        
    }

});

flxCommerceApp.service("attributesProcessManagementService", function ($filter) {

    //Private variables
    var _action = null;
    var _processHelper = null;


    this.initialise = function (data) {
        _processHelper = new processHelper();
        _processHelper.setAttrTypeCount(data[0]);
        _processHelper.setInitAttrType(data[1]);

    };

    this.getCurrentAction = function () {
        return _action;
    }

    this.updateProcessState = function (data) {

        if (_processHelper.getInitAttrType() != data.Value.SelectedAttrType) {

            if (_processHelper.getSelectedAttributeType() != data.Value.SelectedAttrType) {
                _processHelper.setSelectedAttributeType(data.Value.SelectedAttrType)

                //Reduce count
                if (_processHelper.getCurrentAttrTypeCount() != 0) {

                    _processHelper.reduceCurrentAttrTypeCount();
                }

                if (_processHelper.getCurrentAttrTypeCount() != 0) {

                    _action = "resume";
                    return _action;
                }
                else {
                    _action = "complete";
                    return _action;
                }
            }

        }
        else {
            if (_processHelper.getSelectedAttributeType() == null) {
                _processHelper.setSelectedAttributeType(data.Value.SelectedAttrType)

                //Reduce count
                if (_processHelper.getCurrentAttrTypeCount() != 0) {

                    _processHelper.reduceCurrentAttrTypeCount();
                }

                _action = "start";
                return _action;
            }
            else {

                _processHelper.resetRules();
                _processHelper.setSelectedAttributeType(data.Value.SelectedAttrType);

                //Reduce count
                if (_processHelper.getCurrentAttrTypeCount() != 0) {

                    _processHelper.reduceCurrentAttrTypeCount();
                }

                _action = "restart";
                return _action;
            }
        }
    }

    this.storeProcessData = function (data) {
        _attributeData.push({ Key: data.Key, Value: data.Value });
    };

    this.getProcessData = function () {
        return _attributeData;
    };

    this.getProcessData = function (dataType) {
        return _attributeData;
    };

    this.resetRules = function () {
        _processHelper.resetRules();
    }

    var processHelper = function () {

        //Process rule variables
        var _selectedAttributeType = null;
        var _attrTypeCount = -1;
        var _currentAttrTypeCount = -1;
        var _initAttrType = null;
        var _attributeData = [];

        processHelper.prototype.setSelectedAttributeType = function (attributeType) {
            this._selectedAttributeType = attributeType;
        };
        processHelper.prototype.getSelectedAttributeType = function () {
            return this._selectedAttributeType;
        };

        processHelper.prototype.setAttrTypeCount = function (data) {
            this._attrTypeCount = data.Value;
            this._currentAttrTypeCount = this._attrTypeCount;
        };
        processHelper.prototype.getAttrTypeCount = function () {
            return this._attrTypeCount;
        };
        processHelper.prototype.setInitAttrType = function (data) {
            this._initAttrType = data.Value;
        };
        processHelper.prototype.getInitAttrType = function () {
            return this._initAttrType;
        };

        processHelper.prototype.setCurrentAttrTypeCount = function (currentAttrTypeCount) {
            this._currentAttrTypeCount = currentAttrTypeCount;
        };
        processHelper.prototype.getCurrentAttrTypeCount = function () {
            return this._currentAttrTypeCount;
        };
        processHelper.prototype.reduceCurrentAttrTypeCount = function () {
            this._currentAttrTypeCount--;
        }

        processHelper.prototype.resetRules = function () {

            //Reset variables to default state

            //Reset count back to original number
            this._currentAttrTypeCount = this._attrTypeCount;

            //Reset selected attribute type
            this._selectedAttributeType = null;

        }

    };

});


