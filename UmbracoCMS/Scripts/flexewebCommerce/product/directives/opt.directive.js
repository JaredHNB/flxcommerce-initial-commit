﻿//Global vars
window['flxOptionFieldCount'] = 1;


flxCommerceApp.directive('optParent', function ($filter) {
    return {
        restrict: 'AEC',
        compile: function (tElem, tAttrs) {
            return {
                pre: function ($scope, iElem, iAttrs, controller) {
                },
                post: function ($scope, iElem, iAttrs, controller) {
                }
            }
        },
        controller: function ($scope) {
            $scope.$on('AddOption', function (e, option) {
                var existingOption = $filter('filter')($scope.Product.SelectedOptionCollection, { OptionId: option.OptionId });
                if (existingOption.length > 0) {
                    for (var i = 0; i < $scope.Product.SelectedOptionCollection.length; i++) {
                        if (existingOption[0].OptionId == $scope.Product.SelectedOptionCollection[i].OptionId) {
                            $scope.Product.SelectedOptionCollection.splice([i], 1);
                        }
                    }
                }
                if (option.IsCheck) {
                    $scope.Product.SelectedOptionCollection.push(option);
                }
            });

            $scope.$on('DeleteOption', function (e, option) {
                var existingOption = $filter('filter')($scope.Product.SelectedOptionCollection, { OptionId: option.OptionId });
                if (existingOption.length > 0) {
                    for (var i = 0; i < $scope.Product.SelectedOptionCollection.length; i++) {
                        if (existingOption[0].OptionId == $scope.Product.SelectedOptionCollection[i].OptionId) {
                            $scope.Product.SelectedOptionCollection.splice([i], 1);
                        }
                    }
                }
            });
        },
    };

});
flxCommerceApp.directive('optChild', function ($compile) {
    return {
        restrict: 'AEC',
        require: '^optParent',
        scope: [],
        compile: function (tElem, tAttrs) {
            disableElements(tElem[0]);
            return {
                pre: function($scope, iElem, iAttrs, controller) {
                    init(iElem[0], $scope);

                    var optionId = iElem[0].id;
                    if(optionId != undefined && optionId != null && optionId != "") {
                        $scope.Option.OptionId = optionId;
                    }
                    else {
                        throw { name: "OptionIdISNull", message: "Option Id does not exist, look in opt-directive.js to debug" };
                    }
                },
                post: function ($scope, iElem, iAttrs, controller) {
                    iElem.bind('click', function (e) {

                        //Ensure scope on view and in directive are up to date
                        $scope.$digest();

                        if (e.target.name == "optionCheck") {
                            if (e.target.checked) {
                                enableElements(iElem[0]);
                                $scope.Option.IsCheck = true;
                                $scope.$emit('AddOption', $scope.Option);

                            }
                            else {
                                disableElements(iElem[0]);
                                $scope.Option.IsCheck = false;
                                $scope.$emit('DeleteOption', $scope.Option);

                            };
                        }
                    });
                }
            }
        },
        controller: function ($scope) {
            $scope.Option = {
                OptionId: 0,
                IsCheck: false,
                Collection: new Object()
            }
        },
    };
    function init(element, scope) {
        //Make elements angular
        if (element.type != 'checkbox' && 'disabled' in element) {

            //element.setAttribute('opt-child', '');

            if (element.name != undefined && element.name != null && element.name != "") {
                element.setAttribute('ng-model', 'Option.Collection["' + element.name + '"]');
                $compile(element)(scope);
            }
            else {
                element.setAttribute('ng-model', 'Option.Collection["Field ' + window['flxOptionFieldCount'] + '"]');
                window['flxOptionFieldCount']++;
                $compile(element)(scope);
            }

            if (element.children != undefined && element.children.length > 0) {
                for (var i = 0; i < element.children.length; i++) {
                    init(element.children[i], scope);
                }
            }

        }
        else {
            if (element.children != undefined && element.children.length > 0) {
                for (var i = 0; i < element.children.length; i++) {
                    init(element.children[i], scope);
                }
            };
        }

    };
    function disableElements(element) {

        if (element.type != 'checkbox' && 'disabled' in element) {

            element.disabled = true;

            if (element.children != undefined && element.children.length > 0) {
                for (var i = 0; i < element.children.length; i++) {
                    disableElements(element.children[i]);
                }
            }

        }
        else {
            if (element.children != undefined && element.children.length > 0) {
                for (var i = 0; i < element.children.length; i++) {
                    disableElements(element.children[i]);
                }
            };
        }

    };
    function enableElements(element) {

        if (element.type != 'checkbox' && 'disabled' in element) {

            element.disabled = false;

            if (element.children != undefined && element.children.length > 0) {
                for (var i = 0; i < element.children.length; i++) {
                    enableElements(element.children[i]);
                }
            }

        }
        else {
            if (element.children != undefined && element.children.length > 0) {
                for (var i = 0; i < element.children.length; i++) {
                    enableElements(element.children[i]);
                }
            };
        }

    };

});
