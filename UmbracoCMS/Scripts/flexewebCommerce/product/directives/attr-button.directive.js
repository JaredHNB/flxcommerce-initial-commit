﻿flxCommerceApp.directive('attrStandard', function ($http) {
    return {
        restrict: 'AEC',
        require: '^attrParent',
        scope: [],
        compile: function (tElem, tAttrs) {
            return {
                post: function ($scope, iElem, iAttrs, controller) {

                    //On click event
                    iElem.bind('click', function (e) {

                    });
                }
            }
        },

        controller: function ($scope) {

            //Properties
            $scope.isInStock = true;
            $scope.isSelected = false;
            $scope.Type = null;
            $scope.Value = null;

            $scope.$on('enableButton', function (e, attributeData, action) {


            });
        },
    };
});