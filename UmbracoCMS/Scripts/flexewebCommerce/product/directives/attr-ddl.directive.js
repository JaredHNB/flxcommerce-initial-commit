﻿flxCommerceApp.directive('attrDdl', function ($http, $timeout) {
    return {
        restrict: 'AEC',
        require: '^attrParent',
        compile: function (tElem, tAttrs) {
            return {
                post: function ($scope, iElem, iAttrs, controller) {
                    //Initilase event
                    $scope.CurrentAttrType = iAttrs.name;

                    //On click event
                    iElem.bind('change', function (e) {
                        if (!this.disabled) {

                            $timeout(function () {
                                $scope.$digest();
                            });

                            $scope.$broadcast('setIsSelectedToTrue-OptionElem', $scope.CurrentAttrType, this.value);
                            controller.callBroadCast(this.value, $scope.CurrentAttrType, true);


                        }
                    });
                    $scope.$on('sendOption', function (e, optionScope) {

                        if (optionScope.isInStock) {
                            controller.callBroadCast(optionScope.CurrentAttrValue, optionScope.CurrentAttrType, true);
                        }
                    });

                }
            }
        },

        controller: function ($scope) {

            //Properties
            $scope.CurrentAttrValue = null;
            $scope.CurrentAttrType = null;

        },
    };
});
flxCommerceApp.directive('attrOption', function ($http, $timeout) {
    return {
        restrict: 'AEC',
        require: '^attrDdl',
        scope: [],
        compile: function (tElem, tAttrs) {
            window['flxAttributeDataCollection'].push({ AttrType: tAttrs.name, AttrValue: tAttrs.id, AttrElem: tElem[0] });
            return {
                post: function ($scope, iElem, iAttrs, controller) {
                    //Initilase event
                    $scope.CurrentAttrType = iAttrs.name;
                    $scope.CurrentAttrValue = iAttrs.id;

                    $scope.$on('calculateStock', function (e, data, inventoryService) {

                        data.CurrentAttrValue = $scope.CurrentAttrValue;
                        data.CurrentAttrType = $scope.CurrentAttrType;

                        if (data.AttrTypeCount == 1) {
                            $scope.isInStock = inventoryService.isInStock(data);
                        }
                        else {
                            if (data.CurrentAttrType != data.InitAttrType) {
                                if (data.CurrentAttrType != data.SelectedAttrType) {
                                    $scope.isInStock = inventoryService.isInStock(data);
                                }
                            }
                        }

                        if (!$scope.isInStock) {
                            iElem[0].innerHTML = iElem[0].value.concat(' - Out of stock');
                            iElem[0].setAttribute('style', 'color: red;');
                            iElem[0].setAttribute('disabled', 'true');
                        }
                        else {
                            iElem[0].innerHTML = iElem[0].value;
                            iElem[0].removeAttribute('style');
                            iElem[0].removeAttribute('disabled');

                            if (iElem[0].index == 0) {

                                //Pre-select first element in selection if in stock
                                $scope.isSelected = true;
                                $scope.$emit('sendOption', $scope);
                            }

                        };


                    });
                    $scope.$on('setIsSelectedToFalse-AllTypes', function (e, attrType, selectedAttrValue) {

                        if (selectedAttrValue != $scope.CurrentAttrValue) {
                            iElem[0].selected = false;
                            iElem[0].parentElement.selectedIndex = 0;
                            $scope.isSelected = false;
                            $timeout(function () {
                                $scope.$digest();
                            })
                        }
                    });
                    $scope.$on('setIsSelectedToFalse-SingleType', function (e, attrType, selectedAttrValue) {

                        if (attrType == $scope.CurrentAttrType) {
                            if (selectedAttrValue != $scope.CurrentAttrValue) {
                                iElem[0].selected = false;
                                $scope.isSelected = false;
                                $timeout(function () {
                                    $scope.$digest();
                                })

                            }
                        }
                    });
  
                    $scope.$on('setIsSelectedToTrue-OptionElem', function (e, attrType, selectedAttrValue) {

                        if (attrType == $scope.CurrentAttrType) {
                            if (selectedAttrValue == $scope.CurrentAttrValue) {
                                $scope.isSelected = true;

                                //Update the view
                                iElem[0].selected = 'selected';
                            }
                            else {
                                $scope.isSelected = false;

                                //Update the view
                                iElem[0].selected = false;
                            }
                        }
                    });
                    $scope.$on('getSelectedValue', function (e) {

                        if ($scope.isSelected) {
                            window['flxSelectedAttribute'][$scope.CurrentAttrType] = $scope.CurrentAttrValue;
                        }

                    });

                }
            }
        },

        controller: function ($scope) {

            //Properties
            $scope.isInStock = true;
            $scope.isSelected = false;
            $scope.CurrentAttrValue = null;
            $scope.CurrentAttrType = null;
        },
    };
});