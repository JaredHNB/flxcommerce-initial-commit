﻿flxCommerceApp.service("simpleInventoryService", function ($filter) {


    this.getInventoryDataByParentSku = function (parentSku) {
        var stock = 0;
        angular.forEach(parentSku.AttributeBundle, function (data) {

            if (data.skuSuffix == parentSku.CurrentAttrValue) {
                stock =  data.stocklevel;
            }
        });
        return stock;
    };

    this.isInStock = function (data) {

        if (data.CurrentAttrType != data.SelectedAttrType) {
            return isInStock(data.AttributeBundle, data.CurrentAttrValue, $filter);
        }
        else {
            return true;
        }

    }
});

function isInStock(attributes, selectedAttribute, $filter) {

    var attrs = $filter('filter')(attributes, selectedAttribute, true);

    if (attrs.length == 0) {
        return false;
    }
    else {
        if (attrs[0].outOfStock == "True") {
            return false;
        }
        else {
            return true;
        }

    }
    
}

