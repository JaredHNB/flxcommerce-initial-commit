﻿flxCommerceApp.service("inventoryService", function (simpleInventoryService) {

    this.getInventoryDataByParentSku = function (parentSku) {
        return simpleInventoryService.getInventoryDataByParentSku(parentSku);

    }

    this.isInStock = function (data) {
        return simpleInventoryService.isInStock(data);
    };
});