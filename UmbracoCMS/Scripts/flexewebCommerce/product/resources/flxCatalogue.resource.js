﻿flxCommerceApp.factory("flxCatalogueResource", function ($http) {
	return {

	    getOptionsByProductId: function (productId) {
	        return $http.post("/api/CatalogueWebService/GetOptionsByProductId?productId=" + productId)
		},
	}
});