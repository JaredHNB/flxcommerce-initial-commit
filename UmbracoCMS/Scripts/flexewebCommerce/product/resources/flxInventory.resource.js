﻿flxCommerceApp.factory("flxInventoryResource", function ($http) {
	return {

		getInventoryDataByParentSku: function (parentSku) {
		    return $http.get("/api/InventoryWebService/GetInventoryDataByParentSku?parentSku=" + parentSku)
		},
		getInventoryStatusBySku: function (attributes) {
			return $http.post("/api/InventoryWebService/GetInventoryStatusBySku", attributes)
		},

	}
});