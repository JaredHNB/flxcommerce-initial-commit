﻿flxCommerceApp.filter('trustHtml', ['$sce', '$compile', function ($sce, $compile) {
    return function (text, scope) {
            var template = $sce.trustAsHtml(text);
            return $compile(template)(scope);
        };
    }]);