﻿
function fillInShipping(addressId) {

    if (addressId == -1) {
        document.getElementById("shippingAddressID").value = -1;
        document.getElementById("shippingAddress1").value = null;
        document.getElementById("shippingAddress2").value = null;
        document.getElementById("shippingCity").value = null;
        document.getElementById("shippingState").value = null;
        document.getElementById("shippingPostCode").value = null;
        document.getElementById("shippingCountry").value = null;

    } else {

        $.ajax({
            type: "POST",
            url: "/api/AddressWebService/GetAddress",
            data: "{'addressId': '" + addressId + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                document.getElementById("shippingAddressID").value = msg.AddressID;
                document.getElementById("shippingAddress1").value = msg.Address1;
                document.getElementById("shippingAddress2").value = msg.Address2;
                document.getElementById("shippingCity").value = msg.City;
                document.getElementById("shippingState").value = msg.CountyState;
                document.getElementById("shippingPostCode").value = msg.ZipCode;
                document.getElementById("shippingCountry").value = msg.Country;
            },
            failure: function (msg) {
                alert("fail!");
            }
        });
    }
};

function fillInBilling(addressId) {

    if (addressId == -1) {
        document.getElementById("billingAddressID").value = -1;
        document.getElementById("billingAddress1").value = null;
        document.getElementById("billingAddress2").value = null;
        document.getElementById("billingCity").value = null;
        document.getElementById("billingState").value = null;
        document.getElementById("billingPostCode").value = null;
        document.getElementById("billingCountry").value = null;

    } else {

        $.ajax({
            type: "POST",
            url: "/api/AddressWebService/GetAddress",
            data: "{'addressId': '" + addressId + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                document.getElementById("billingAddressID").value = msg.AddressID;
                document.getElementById("billingAddress1").value = msg.Address1;
                document.getElementById("billingAddress2").value = msg.Address2;
                document.getElementById("billingCity").value = msg.City;
                document.getElementById("billingState").value = msg.CountyState;
                document.getElementById("billingPostCode").value = msg.ZipCode;
                document.getElementById("billingCountry").value = msg.Country;
            },
            failure: function (msg) {
                alert("fail!");
            }
        });
    }

};

function CountrySelectorShipping(objDropDown) {

    if (objDropDown.value == "US") {
        //If USA then change the state input to dropdown and set names for ViewModel
        $("#shippingState").prop('required', false);
        $("#shippingState").prop('name', 'old');
        $("#standardShippingState").hide();
        $("#shippingStatediv").show();
        $("#shippingStateUSSelect").prop('name', 'shippingState');
        $("#shippingStateUSSelect").prop('required', true);
    } else {

        $("#shippingState").prop('required', true);
        $("#shippingState").prop('name', 'shippingState');
        $("#standardShippingState").show();
        $("#shippingStatediv").hide();
        $("#shippingStateUSSelect").prop('name', 'old');
        $("#shippingStateUSSelect").prop('required', false);

    }

};

function CountrySelectorBilling(objDropDown) {

    if (objDropDown.value == "US") {
        //If USA then change the state input to dropdown and set names for ViewModel
        $("#billingState").prop('required', false);
        $("#billingState").prop('name', 'old');
        $("#standardBillingState").hide();
        $("#billingStatediv").show();
        $("#billingStateUSSelect").prop('name', 'billingState');
        $("#billingStateUSSelect").prop('required', true);
    } else {

        $("#billingState").prop('required', true);
        $("#billingState").prop('name', 'billingState');
        $("#standardBillingState").show();
        $("#billingStatediv").hide();
        $("#billingStateUSSelect").prop('name', 'old');
        $("#billingStateUSSelect").prop('required', false);

    }

};

