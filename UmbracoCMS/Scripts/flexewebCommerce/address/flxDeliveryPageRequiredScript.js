﻿
//This will check if the check box is clicked
$('#differentAddresses').click(function () {
    var billingForm = document.getElementById("billingForm");
    $("select#billingCountry").find("option#1").attr("disable", false);
    $("select#billingCountry").find("option#1").attr("selected", true);

    //if checkbox is clicked then make the textbox required
    if ($('#differentAddresses').prop('checked')) {
        $("#billingAddress1").prop('required', true);
        $("#billingCity").prop('required', true);
        $("#billingState").prop('required', true);
        $("#billingPostCode").prop('required', true);
        $("#billingCountry").prop('required', true);
        $("#billingCountrySelect").prop('required', true);
    } else {
        $("#billingAddress1").prop('required', false);
        $("#billingAddress2").prop('required', false);
        $("#billingCity").prop('required', false);
        $("#billingState").prop('required', false);
        $("#billingPostCode").prop('required', false);
        $("#billingCountry").prop('required', false);
        $("#billingCountrySelect").prop('required', false);
    }

});



function attributeSupported(attribute) {
    return (attribute in document.createElement("required"));
}

//Required attribute fix for Safari
$('#formTemplate').submit(function (event) {
    if (!attributeSupported("required")) {
        //If required attribute is not supported or browser is Safari (Safari thinks that it has this attribute, but it does not work), then check all fields that has required attribute
        $("#formTemplate [required]").each(function (index) {
            if (!$(this).val()) {
                //If at least one required value is empty, then ask to fill all required fields.
                alert("Please fill all required fields.");
                event.preventDefault();
                return false;
            }
        });
    }
    return true;
});

