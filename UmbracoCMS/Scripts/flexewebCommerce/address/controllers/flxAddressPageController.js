﻿flxCommerceApp.controller('addressController', function ($scope, $http, $window) {

    $scope.user = {};
    $scope.user.IsShippingDefault = false;
    $scope.user.IsBillingDefault = false;

    $scope.address = {}
    $scope.address.IsShippingDefault = false;
    $scope.address.IsBillingDefault = false;
    

    $scope.addAddress = function (user) {
        //Build the address from the form
        $scope.user.IsShippingDefault = document.getElementById('IsShippingDefault').checked;
        $scope.user.IsBillingDefault = document.getElementById('IsBillingDefault').checked;

       
        $scope.user.Address1 =  $("#billingAddress1").val();
        $scope.user.Address2 =  $("#billingAddress2").val();
        $scope.user.City =  $("#billingCity").val();
        $scope.user.CountyState = $("[name='billingState']").val();
        $scope.user.ZipCode =  $("#billingPostCode").val();
        

        $http({
            method: 'POST',
            url: '/api/AddressWebService/CreateAddress',
            data: user,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
        }).success(function (data, status, headers, config) {                    
            $window.location.href = window.location.href;
        });
    };

    $scope.saveAddress = function (addressID,billing) {
        
        $scope.address.AddressID = addressID;
        $scope.address.FirstName = document.getElementById("firstnameedit" + addressID).value;
        $scope.address.LastName = document.getElementById("lastnameedit" + addressID).value;
        $scope.address.Address1 = document.getElementById("address1edit" + addressID).value;
        $scope.address.Address2 = document.getElementById("address2edit" + addressID).value;
        $scope.address.City = document.getElementById("cityedit" + addressID).value;
               
        $scope.address.ZipCode = document.getElementById("zipcodeedit" + addressID).value;
        $scope.address.Phone = document.getElementById("phoneedit" + addressID).value;
        if (billing == "b") {
            $scope.address.IsShippingDefault = document.getElementById('IsShippingDefaultb' + addressID).checked;
            $scope.address.IsBillingDefault = document.getElementById('IsBillingDefaultb' + addressID).checked;
            $scope.address.Country = document.getElementById("countryselectorb" + addressID).value;
        } else {
            $scope.address.IsShippingDefault = document.getElementById('IsShippingDefault' + addressID).checked;
            $scope.address.IsBillingDefault = document.getElementById('IsBillingDefault' + addressID).checked;
            $scope.address.Country = document.getElementById("countryselector" + addressID).value;
        }

        if ($scope.address.Country == "US" && billing == "b") {
                        
            $scope.address.CountyState = document.getElementById("countyselectorb" + addressID).value;

        } else if ($scope.address.Country == "US" && billing != "b") {
            $scope.address.CountyState = document.getElementById("countyselector" + addressID).value;

        } else {

            $scope.address.CountyState = document.getElementById("countystateedit" + addressID).value;
        }

        $http({
            method: 'POST',
            url: '/api/AddressWebService/UpdateAddress',
            data: $scope.address,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
        }).success(function (data, status, headers, config) {
            $window.location.href = window.location.href;
        });
    }

    $scope.cancel = function () {

        $window.location.href = window.location.href;
    }

    $scope.delete = function (addressID) {
        $scope.addresstodelete = {};
        $scope.addresstodelete.AddressID = addressID;
        $http({
            method: 'POST',
            url: '/api/AddressWebService/DeleteAddress',
            data: $scope.addresstodelete,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
        }).success(function (data, status, headers, config) {
            $window.location.href = window.location.href;
        });

        $window.location.href = window.location.href;
    }
});



