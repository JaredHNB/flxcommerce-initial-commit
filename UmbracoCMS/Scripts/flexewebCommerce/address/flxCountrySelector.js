﻿function CountrySelectorShipping(objDropDown) {

    var shippingpostcodeinput = document.getElementById("shippingpostcodeinput");
    var shippingForm = document.getElementById("shippingForm");

    if (objDropDown.value != "GB") {
        shippingpostcodeinput.style.display = 'none';
        shippingForm.style.display = 'inherit';
        if (objDropDown.value == "US") {
            //If USA then change the state input to dropdown and set names for ViewModel
            $("#shippingState").prop('required', false);
            $("#shippingState").prop('name', 'old');
            $("#standardShippingState").hide();
            $("#shippingStatediv").show();
            $("#shippingStateUSSelect").prop('name', 'shippingState');
            $("#shippingStateUSSelect").prop('required', true);
        } else {
            //Make sure the form has the correct state input
            $("#shippingState").prop('required', true);
            $("#shippingState").prop('name', 'shippingState');
            $("#standardShippingState").show();
            $("#shippingStatediv").hide();
            $("#shippingStateUSSelect").prop('name', 'old');
            $("#shippingStateUSSelect").prop('required', false);
        }
    } else {
        shippingpostcodeinput.style.display = 'inherit';
        //shippingForm.style.display = 'none';
        //Make sure the form has the correct state input
        $("#shippingState").prop('required', true);
        $("#shippingState").prop('name', 'shippingState');
        $("#standardShippingState").show();
        $("#shippingStatediv").hide();
        $("#shippingStateUSSelect").prop('name', 'old');
        $("#shippingStateUSSelect").prop('required', false);

    }
}

function CountrySelectorBilling(objDropDown) {

    var billingpostcodeinput = document.getElementById("billingpostcodeinput");
    var billingForm = document.getElementById("billingForm");

    if (objDropDown.value != "GB") {
        billingpostcodeinput.style.display = 'none';
        if (objDropDown.value == "US") {
            //If USA then change the state input to dropdown and set names for ViewModel
            $("#billingState").prop('required', false);
            $("#billingState").prop('name', 'old');
            $("#standardBillingState").hide();
            $("#billingStatediv").show();
            $("#billingStateUSSelect").prop('name', 'billingState');
            $("#billingStateUSSelect").prop('required', true);
        } else {
            //Make sure the form has the correct state input
            $("#billingState").prop('required', true);
            $("#billingState").prop('name', 'billingState');
            $("#standardBillingState").show();
            $("#billingStatediv").hide();
            $("#billingStateUSSelect").prop('name', 'old');
            $("#billingStateUSSelect").prop('required', false);

        }

        //billingpostcodeinput.style.display = 'none';
        billingForm.style.display = 'inherit';

    } else {

        var addresstickbox = document.getElementById("differentAddresses");
        if (addresstickbox.checked != false) {
            billingpostcodeinput.style.display = 'inherit';
            //billingForm.style.display = 'none'
            //Make sure the form has the correct state input
            $("#billingState").prop('required', true);
            $("#billingState").prop('name', 'billingState');
            $("#standardBillingState").show();
            $("#billingStatediv").hide();
            $("#billingStateUSSelect").prop('name', 'old');
            $("#billingStateUSSelect").prop('required', false);
        }

    }




}
