﻿
function showAddressForm() {

    var addressForm = document.getElementById("addressForm");
    var showAddressFormbtn = document.getElementById("showAddressFormbtn");
  

    if (addressForm.style.display = "inherit") {

        showAddressFormbtn.style.display = 'none';
        addressForm.style.display = 'inherit';

    } else {

        showAddressFormbtn.style.display = 'inherit';
        addressForm.style.display = 'none'

    }




}

function editAddress(addressID, billing) {

    //Get all the variables from the page
    var firstname = document.getElementById("firstname"+ billing +  addressID).firstChild;
    var lastname = document.getElementById("lastname" + billing +  addressID);
    var address1 = document.getElementById("address1" + billing +  addressID);
    var address2 = document.getElementById("address2" + billing +  addressID);
    var city = document.getElementById("city" + billing +  addressID);
    var countystate = document.getElementById("countystate" + billing +  addressID);
    var country = document.getElementById("country" + billing +  addressID);
    var zipcode = document.getElementById("zipcode" + billing +  addressID);
    var phone = document.getElementById("phone" + billing +  addressID);
    var editaddress = document.getElementById("editaddress" + billing +  addressID);
    var saveaddress = document.getElementById("saveaddress" + billing +  addressID);
    var tsymbol = document.getElementById("t" + billing +  addressID);
    var checkboxes = document.getElementById("checkboxes" + billing +  addressID);
    var cancel = document.getElementById("cancel" + billing +  addressID);
    var deletebtn = document.getElementById("delete" + billing +  addressID);
    var countrysl = document.getElementById("countrysl" + billing +  addressID);
    var countryselector = document.getElementById("countryselector" + billing +  addressID);
    var countysl = document.getElementById("countysl" + billing +  addressID);
    var countyselector = document.getElementById("countyselector" + billing +  addressID);
   
    //Replace all the HTML with input boxes and a country dropdown box. Make the save+cancel buttons replace the edit+delete buttons
    $(firstname).replaceWith('<input type="text" id="firstnameedit' + addressID + '" ' + 'value="' + firstname.textContent + '" class="form-control" style="height:35px;margin:0px 0px 1px 0px">');
    $(lastname).replaceWith('<input type="text" id="lastnameedit' + addressID + '" ' + 'value="' + lastname.innerText + '" class="form-control" style="height:35px;margin:0px 0px 1px 0px">');
    $(address1).replaceWith('<input type="text" id="address1edit' + addressID + '" ' + 'value="' + address1.innerText + '" class="form-control" style="height:35px;margin:0px 0px 1px 0px">');
    $(address2).replaceWith('<input type="text" id="address2edit' + addressID + '" ' + 'value="' + address2.innerText + '" class="form-control" style="height:35px;margin:0px 0px 1px 0px">');
    $(city).replaceWith('<input type="text" id="cityedit' + addressID + '" ' + 'value="' + city.innerText + '" class="form-control" style="height:35px;margin:0px 0px 1px 0px">');
    if (country.innerText.replace(/\s/g, "") == "US")
    {
        countyselector.value = countystate.innerText.replace(/\s/g, "");
        $(countystate).hide();
        $(countysl).show();

    }else {
        $(countystate).replaceWith('<input type="text" id="countystateedit' + addressID + '" ' + 'value="' + countystate.innerText + '" class="form-control" style="height:35px;margin:0px 0px 1px 0px">');
    }
    //$(country).replaceWith('<input type="text" id="countryedit ' + addressID + '" ' + 'value="' + country.innerText + '" class="form-control" style="height:35px;margin:0px 0px 1px 0px">');
    $(country).hide();
    $(zipcode).replaceWith('<input type="text" id="zipcodeedit' + addressID + '" ' + 'value="' + zipcode.innerText + '"class="form-control" style="height:35px;margin:0px 0px 1px 0px">');
    $(tsymbol).replaceWith('<input type="text" id="phoneedit' + addressID + '" ' + 'value="' + phone.innerText + '" class="form-control" style="height:35px;margin:0px 0px 1px 0px">');
    $(editaddress).hide();
    $(saveaddress).show();
    $(checkboxes).show();
    $(cancel).show();
    $(deletebtn).hide();
    countryselector.value = country.innerText.replace(/\s/g, "");
    $(countrysl).show();
     
}
