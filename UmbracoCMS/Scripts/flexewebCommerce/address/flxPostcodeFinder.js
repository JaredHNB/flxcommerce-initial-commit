﻿
function getAddressList(form) {
    var SearchTerm = document.getElementById(form + "Postcode").value;
    var link = document.getElementById('manual'+ form);
    link.style.display = 'none';
    $.ajax({
        type: "POST",
        url: "/api/PostcodeWebService/GetAddressListFromPostcode",
        data: "{'postcode': '" + SearchTerm + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {

            if (msg.length > 0){
            var dropdown = document.getElementById(form + "Dropdown");
            dropdown.style.display = '';
            var elem = document.getElementById(form + 'Form');
            elem.style.display = '';
            dropdown.options.length = 0;
            dropdown.options.add(new Option("Select an address:", ""));
            for (var j = 0; j < msg.length; j++) {
                dropdown.options.add(new Option(msg[j].Address1 + ", " + msg[j].Address2, msg[j].AddressID));
            }
            }else{
                alert("No addresses found!");
                var elem = document.getElementById(form + 'Form');
                elem.style.display = ''; 
               
            }
        },
        failure: function (msg) {
            alert("fail!");
        }
    });
};

function getAddress(form) {
    var SearchTerm = document.getElementById(form + "Dropdown").value;

    $.ajax({
        type: "POST",
        url: "/api/PostcodeWebService/GetAddressById",
        data: "{'addressId': '" + SearchTerm + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {

            if (msg == null) {
                alert("Sorry, no matching items found");
            }
            else {
                
                document.getElementById(form + "Address1").value = msg.Address1;
                document.getElementById(form + "City").value = msg.City;
                document.getElementById(form + "State").value = msg.CountyState;
                document.getElementById(form + "PostCode").value = msg.ZipCode;

                var dropdown = document.getElementById(form + "Dropdown");

                dropdown.style.display = 'none';
                dropdown.options.length = 0;
                var link = document.getElementById('manual'+ form);
                link.style.display = 'none';
            }


        }
    });
}

function showAddressform(form) {

    var formtodisplay = form + "Form";
    var elem = document.getElementById(formtodisplay);
    elem.style.display = '';

}