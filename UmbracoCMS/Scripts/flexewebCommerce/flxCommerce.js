//Global Variables
var isValid = true;
var colour = null;
var size = null;
var productId = null;
var selectedColour = null;
var selectedSize = null;
var uniqueId = null;

function getURLParameter(name) {
    return decodeURIComponent((new RegExp("[?|&]" + name + "=" + "([^&;]+?)(&|#|;|$)").exec(location.search) || [, ""])[1].replace(/\+/g, "%20")) || null
}

$(document).ready(function () {

    //$("table.cfgBasket  td.qty").append('<div class="inc button">+</div><div class="dec button">-</div>');
    //$("table.cfgBasket  td.qty").append('<div class="delete">x</div>');

    $(".button").on("click", function () {

        var $button = $(this);
        var oldValue = $button.parent().find("input").val();

        if ($button.text() == "+") {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            // Don't allow decrementing below zero
            if (oldValue > 0) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
            }
        }

        $button.parent().find("input").val(newVal);

    });
    
    $(".delete").on("click", function () {

        var $del = $(this);

        $.ajax({
            type: "POST",
            url: "/webservices/OrderWebService.asmx/DeleteOrderLine",
            data: "{'orderLineId': '" + $del.Id + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                //var resp = JSON.parse(msg);
                // Replace the div's content with the page method's return.
                //$(".basketTotal").text(msg.d);
                $(".basketTotal").text(msg.Currency.Symbol + msg.OrderTotal.toFixed(2));
                $(".basketQty").text(msg.OrderLines.ItemsCount);
            },
            failure: function (msg) {
                alert("fail!");
            }
        });
    });


    $(".getProductOveriew").click(function () {
        window['uniqueId'] = $(this).attr("data-uniqueid");
        var productId = $(this).attr("data-productid");
        var altTemplate = "flxIframeProduct";
        var productUrl = $(this).attr("data-productUrl") + "?alttemplate=" + altTemplate;
        $("#iframe-" + window['uniqueId']).attr("src", productUrl);

    });
});

function refreshUI(order) {
    $(".basketTotal").text(msg);
}

function getAttVal(attVal) {
    if (attVal == null) {
        window['isValid'] = false;
        return '';
    }
    else {
        return attVal;
    }
    return
}

