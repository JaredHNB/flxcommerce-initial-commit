﻿flxCommerceApp.service("processManagementService", function (attributesProcessManagementService) {
    
    this.initialise = function (data) {
        return attributesProcessManagementService.initialise(data);
    };

    this.getCurrentAction = function () {
        return attributesProcessManagementService.getCurrentAction();
    };

    this.getAction = function (data) {
        return attributesProcessManagementService.getAction(data);
    };

    this.storeProcessData = function (data) {
        return attributesProcessManagementService.storeProcessData(data);

    };

    this.getProcessData = function () {
        return attributesProcessManagementService.getProcessData();

    };

});