﻿flxCommerceApp.service("attributesProcessManagementService", function ($filter) {

    //Private variables
    var _action = null;
    var _processHelper = null;


    this.initialise = function (data) {
        _processHelper = new processHelper();
        _processHelper.setAttrTypeCount(data[0]);
        _processHelper.setInitAttrType(data[1]);
        
    };

    this.getCurrentAction = function () {
        return _action;
    }

    this.updateProcessState = function (data) {

        if (_processHelper.getInitAttrType() != data.Value.SelectedAttrType) {

            if (_processHelper.getSelectedAttributeType() != data.Value.SelectedAttrType) {
                _processHelper.setSelectedAttributeType(data.Value.SelectedAttrType)

                //Reduce count
                if (_processHelper.getCurrentAttrTypeCount() != 0) {

                    _processHelper.reduceCurrentAttrTypeCount();
                }

                if (_processHelper.getCurrentAttrTypeCount() != 0) {

                    _action = "resume";
                    return _action;
                }
                else {
                    _action = "complete";
                    return _action;
                }
            }

        }
        else {
            if (_processHelper.getSelectedAttributeType() == null) {
                _processHelper.setSelectedAttributeType(data.Value.SelectedAttrType)

                //Reduce count
                if (_processHelper.getCurrentAttrTypeCount() != 0) {

                    _processHelper.reduceCurrentAttrTypeCount();
                }

                _action = "start";
                return _action;
            }
            else {

                _processHelper.resetRules();
                _processHelper.setSelectedAttributeType(data.Value.SelectedAttrType);

                //Reduce count
                if (_processHelper.getCurrentAttrTypeCount() != 0) {

                    _processHelper.reduceCurrentAttrTypeCount();
                }

                _action = "restart";
                return _action;
            }
        }
    }

    this.storeProcessData = function (data) {
        _attributeData.push({ Key: data.Key, Value: data.Value });
    };

    this.getProcessData = function () {
        return _attributeData;
    };

    this.getProcessData = function (dataType) {
        return _attributeData;
    };

    this.resetRules = function() {
        _processHelper.resetRules();
    }
});

var processHelper = function () {

    //Process rule variables
    var _selectedAttributeType = null;
    var _attrTypeCount = -1;
    var _currentAttrTypeCount = -1;
    var _initAttrType = null;
    var _attributeData = [];

    processHelper.prototype.setSelectedAttributeType = function(attributeType) {
        this._selectedAttributeType = attributeType;
    };
    processHelper.prototype.getSelectedAttributeType = function () {
        return this._selectedAttributeType;
    };

    processHelper.prototype.setAttrTypeCount = function (data) {
        this._attrTypeCount = data.Value;
        this._currentAttrTypeCount = this._attrTypeCount;
    };
    processHelper.prototype.getAttrTypeCount = function () {
        return this._attrTypeCount;
    };
    processHelper.prototype.setInitAttrType = function (data) {
        this._initAttrType = data.Value;
    };
    processHelper.prototype.getInitAttrType = function () {
        return this._initAttrType;
    };

    processHelper.prototype.setCurrentAttrTypeCount = function (currentAttrTypeCount) {
        this._currentAttrTypeCount = currentAttrTypeCount;
    };
    processHelper.prototype.getCurrentAttrTypeCount = function () {
        return this._currentAttrTypeCount;
    };
    processHelper.prototype.reduceCurrentAttrTypeCount = function () {
         this._currentAttrTypeCount--;
    }

    processHelper.prototype.resetRules = function () {

        //Reset variables to default state

        //Reset count back to original number
        this._currentAttrTypeCount = this._attrTypeCount;

        //Reset selected attribute type
        this._selectedAttributeType = null;

    }

};