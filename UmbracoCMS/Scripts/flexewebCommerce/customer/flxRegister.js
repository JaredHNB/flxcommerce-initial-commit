
$('#CreateAccount').click(function (e) {

    e.preventDefault();

    if ($('#Password').val() != $('#ConfirmPassword').val()) {
        alert("Passwords must match");
    }
    else {


        var customer =
        {
            'firstname': $('#FirstName').val(),
            "lastname": $('#LastName').val(),
            "username": $('#Email').val(),
            "password": $('#Password').val(),
            "email": $('#Email').val(),
            "telephone": $('#Telephone').val()
        }
        $("#loading").show();
        $.post('/api/CustomerWebService/AddCustomer', customer,
        function () {
            $('#CreateAccount').off("click", CreateAccount);
        })
        .done(function (data) {
            if (data == 1) {
                $("#failure").show();
                $("#loading").hide();
            } else {
                $("#loading").hide();
                var returnurl = getURLParameter("returnurl");

                if (returnurl == null) {

                    var returnurlform = $("#returnurl").val();

                    if (returnurlform === null || returnurlform === undefined) {
                        returnurl = "/"
                    }
                    else {
                        returnurl = returnurlform;
                    }

                }

                location.href = "/" + returnurl;
            }
        })
    }
});
