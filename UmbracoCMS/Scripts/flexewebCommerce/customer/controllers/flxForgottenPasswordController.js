﻿flxCommerceApp.controller('forgottenPassword', function ($scope, $http, $location, $window) {

    //initialise var
    $scope.passwordPattern = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
    $scope.user = {};
    $scope.emailsent = false;
    $scope.emailerror = false;
    $scope.membernotfound = false;
    $scope.resetPasswordBtn = true
    $scope.loginBtn = false;
    $scope.passwordChanged = false;
    $scope.guidError = false;
    $scope.forgottenBtn = false;
    $scope.sendEmailbtn = true;
   

    //Send email function
    $scope.sendEmail = function (user) {
        //Clear all previous error messages
        $scope.emailsent = false;
        $scope.emailerror = false;
        $scope.membernotfound = false;
        $scope.sendEmailbtn = true;
        $http({
            method: 'POST',
            url: '/api/CustomerWebService/GenerateResetToken',
            data: user,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
        }).success(function (data, status, headers, config) {
            //Check the response and show the customer an error message
            if (JSON.parse(data) == "emailsent") {
                $scope.emailsent = true;
                $scope.sendEmailbtn = false;
            } else if (JSON.parse(data) == "emailerror") {
                $scope.emailerror = true;
            } else if (JSON.parse(data) == "Nomemberfound") {
                $scope.membernotfound = true;
            }
        });
    };

    $scope.updatePassword = function (password) {
        //Build up the customer model to send to the API
        $scope.customer = {};
        $scope.customer.email = $location.search().email;
        $scope.customer.resetGuid = $location.search().resetguid;
        $scope.customer.newPassword = password;
        $scope.guidError = false;
        $scope.forgottenBtn = false;

        $http({
            method: 'POST',
            url: '/api/CustomerWebService/UpdatePassword',
            data: $scope.customer,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
        }).success(function (data, status, headers, config) {
            //Check the response and show the customer an error message
            if (JSON.parse(data) == "passwordupdated") {
                //hide reset button - show login button
                $scope.resetPasswordBtn = false;
                $scope.loginBtn = true;
                $scope.passwordChanged = true;
            } else if (JSON.parse(data) == "guidsdontmatch") {
                //hide rest button, show forgotten password button
                $scope.resetPasswordBtn = false;
                $scope.guidError = true;
                $scope.forgottenBtn = true;
            } else if (JSON.parse(data) == "Nomemberfound") {
                //hide rest button, show forgotten password button
                $scope.resetPasswordBtn = false;
                $scope.guidError = true;
                $scope.forgottenBtn = true;
            }
        });
    };

    $scope.forwardLogin = function () {
        $window.location.href = "/login/";

    };

    $scope.forwardForgotten = function () {
        $window.location.href = "/forgottenpassword/";

    };

});

flxCommerceApp.directive("ngEquals", function () {
    var directive = {};

    directive.restrict = 'A';
    directive.require = 'ngModel';
    directive.scope = {
        original: '=ngEquals'
    };

    directive.link = function (scope, elm, attrs, ngModel) {
        ngModel.$parsers.unshift(function (value) {
            ngModel.$setValidity('equals', scope.original === value);
            return value;
        });
    };

    return directive;
});



