﻿flxCommerceApp.controller('profileController', function ($scope, $http, $window) {

    $scope.profile = {}
    $scope.passwordPattern = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
    $scope.passwordupdated = false;
    $scope.message = "You can change your password below";

    $scope.saveProfile = function () {


        $scope.profile.firstname = document.getElementById("firstnameedit").value;
        $scope.profile.lastname = document.getElementById("lastnameedit").value;
        $scope.profile.telephone = document.getElementById("telephoneedit").value;

        $http({
            method: 'POST',
            url: '/api/CustomerWebService/UpdateCustomer',
            data: $scope.profile,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
        }).success(function (data, status, headers, config) {

            $window.location.reload();
        });

    };


    $scope.updatePassword = function () {
        //Build up the customer model to send to the API
        $scope.customer = {};
        $scope.customer.currentPassword = $scope.currentpassword;
        $scope.customer.newPassword = $scope.password;

        $http({
            method: 'POST',
            url: '/api/CustomerWebService/UpdateCurrentCustomersPassword',
            data: $scope.customer,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
        }).success(function (data, status, headers, config) {
            if (data == "passwordupdated") {
                $scope.message = "Password updated successfully";
            } else if (data == "passwordsdontmatch") {
                $scope.message = "Current password was entered incorrectly";

            }

        });
    };


});





flxCommerceApp.directive("ngEquals", function () {
    var directive = {};

    directive.restrict = 'A';
    directive.require = 'ngModel';
    directive.scope = {
        original: '=ngEquals'
    };

    directive.link = function (scope, elm, attrs, ngModel) {
        ngModel.$parsers.unshift(function (value) {
            ngModel.$setValidity('equals', scope.original === value);
            return value;
        });
    };

    return directive;
});
