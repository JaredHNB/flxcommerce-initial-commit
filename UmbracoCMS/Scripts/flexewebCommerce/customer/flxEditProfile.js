﻿function editProfile() {  
    
    $(firstnameedit).prop("readonly", false);
    $(lastnameedit).prop("readonly", false);
    $(telephoneedit).prop("readonly", false);
   
    $(editprofile).hide();
    $(save).show();
    $(cancelbutton).show();
    $(changepassword).hide();

};


function cancel() {

    $(firstnameedit).prop("readonly", true);
    $(lastnameedit).prop("readonly", true);
    $(telephoneedit).prop("readonly", true);
    

    
    $(editprofile).show();
    $(save).hide();
    $(cancelbutton).hide();
    $(changepassword).show();

};


