

function Login(e) {

    e.preventDefault();

    var customer =
    {
        "loginUsername": $("#Email").val(),
        "loginPassword": $("#Password").val(),
    }

    $.post("/api/SecurityWebService/Login", customer,
    function () {
        $("#LoginBtn").off("click", Login);
    })
    .done(function (data) {

        if (data == true) {

            var returnurl = getURLParameter("returnurl");

            if (returnurl == null) {

                var returnurlform = $("#returnurl").val();

                if (returnurlform === null || returnurlform === undefined)
                {
                    returnurl = "/"
                }
                else
                {
                    returnurl = returnurlform;
                }

            }

            window.location.href = returnurl;
        }
        else
        {
            $('#initialgreeting').hide();
            $('#error').show();
            $("#LoginBtn").on("click", Login);
        }

    })

}

$("#LoginBtn").on("click", Login);

