[
    {
        "name": "Rich text editor",
        "alias": "rte",
        "view": "rte",
        "icon": "icon-article"
    },
    {
        "name": "Image",
        "alias": "media",
        "view": "media",
        "icon": "icon-picture"
    },
    {
        "name": "Macro",
        "alias": "macro",
        "view": "macro",
        "icon": "icon-settings-alt"
    },
    {
        "name": "Embed",
        "alias": "embed",
        "view": "embed",
        "icon": "icon-movie-alt"
    },
    {
        "name": "Quote",
        "alias": "quote",
        "view": "textstring",
        "icon": "icon-quote",
        "config": {
            "style": "border-left: 3px solid #ccc; padding: 10px; color: #ccc; font-family: serif; font-style: italic; font-size: 18px",
            "markup": "<blockquote>#value#</blockquote>"
        }
    },
    {
        "name": "Headline H1",
        "alias": "headline",
        "view": "textstring",
        "icon": "icon-height",
        "config": {
            "style": "font-size: 36px; line-height: 45px; font-weight: bold",
            "markup": "<h1>#value#</h1>"
        }
    },
    {
        "name": "Headline H2",
        "alias": "headlineH2",
        "view": "textstring",
        "icon": "icon-height",
        "config": {
            "style": "font-size: 36px; line-height: 45px; font-weight: bold",
            "markup": "<h2>#value#</h2>"
        }
    },
    {
        "name": "Buttons",
        "alias": "buttons",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-settings-alt",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Button 1 Name",
                    "alias": "button1Name",
                    "propretyType": {},
                    "dataType": "9db4dea5-0918-4f64-8345-472ba85220f2"
                },
                {
                    "name": "Button 1 Link",
                    "alias": "button1Link",
                    "propretyType": {},
                    "dataType": "fd1e0da5-5606-4862-b679-5d0cf3a52a59"
                },
                {
                    "name": "Button 1 Style",
                    "alias": "button1style",
                    "propretyType": {},
                    "dataType": "92897bc6-a5f3-4ffe-ae27-f2e7e33dda49"
                },
                {
                    "name": "Button 2 Name",
                    "alias": "button2Name",
                    "propretyType": {},
                    "dataType": "9db4dea5-0918-4f64-8345-472ba85220f2"
                },
                {
                    "name": "Button 2 Link",
                    "alias": "button2Link",
                    "propretyType": {},
                    "dataType": "fd1e0da5-5606-4862-b679-5d0cf3a52a59"
                },
                {
                    "name": "Button 3 Name",
                    "alias": "button3Name",
                    "propretyType": {},
                    "dataType": "9db4dea5-0918-4f64-8345-472ba85220f2"
                },
                {
                    "name": "Button 3 Link",
                    "alias": "button3Link",
                    "propretyType": {},
                    "dataType": "fd1e0da5-5606-4862-b679-5d0cf3a52a59"
                },
                {
                    "name": "Social Icons",
                    "alias": "socialIcons",
                    "propretyType": {},
                    "dataType": "92897bc6-a5f3-4ffe-ae27-f2e7e33dda49"
                },
                {
                    "name": "Social Link",
                    "alias": "socialLink",
                    "propretyType": {},
                    "dataType": "fd1e0da5-5606-4862-b679-5d0cf3a52a59",
                    "description": "Page reference for all social icon links. Leave blank for current page."
                },
                {
                    "name": "Link to Social Site",
                    "alias": "linkToSocialSite",
                    "propretyType": {},
                    "dataType": "92897bc6-a5f3-4ffe-ae27-f2e7e33dda49",
                    "description": "Makes the link go offsite to social or share this page to social as a link"
                }
            ],
            "renderInGrid": "1",
            "frontView": "buttons"
        }
    },
    {
        "name": "Inline Class",
        "alias": "inlineClass",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-tab-key",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Add Class",
                    "alias": "addClass",
                    "propretyType": {},
                    "dataType": "9db4dea5-0918-4f64-8345-472ba85220f2"
                }
            ],
            "renderInGrid": "0",
            "frontView": ""
        }
    },
    {
        "name": "test",
        "alias": "test",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-settings-alt",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "textbox",
                    "alias": "textbox",
                    "propretyType": {},
                    "dataType": "9db4dea5-0918-4f64-8345-472ba85220f2"
                }
            ],
            "renderInGrid": "1",
            "frontView": ""
        }
    },
    {
        "name": "Image Link",
        "alias": "Media Link",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-forms-libreoffice",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Media Link",
                    "alias": "mediaLink",
                    "propretyType": {},
                    "dataType": "fd1e0da5-5606-4862-b679-5d0cf3a52a59"
                },
                {
                    "name": "New Window?",
                    "alias": "newWindow",
                    "propretyType": {},
                    "dataType": "8618fca1-1d4c-4569-8670-7ca6eb993bd2"
                },
                {
                    "name": "Image",
                    "alias": "image",
                    "propretyType": {},
                    "dataType": "135d60e0-64d9-49ed-ab08-893c9ba44ae5"
                },
                {
                    "name": "Typed Link",
                    "alias": "typedLink",
                    "propretyType": {},
                    "dataType": "9db4dea5-0918-4f64-8345-472ba85220f2"
                },
                {
                    "name": "Alt Text",
                    "alias": "altText",
                    "propretyType": {},
                    "dataType": "9db4dea5-0918-4f64-8345-472ba85220f2"
                }
            ],
            "renderInGrid": "1",
            "frontView": "ImageLink"
        }
    }
]