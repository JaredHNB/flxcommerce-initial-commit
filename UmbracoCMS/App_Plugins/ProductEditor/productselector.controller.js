﻿angular.module("umbraco").controller("flx.productSelectorController",
    function ($scope, $http, $q, editorState) {

        //Make Products scope an array
        $scope.Products = new Array();

        var def1 = $q.defer();
        var def2 = $q.defer();

        def1.promise.then(function (data) {

            //Add api data to UmbracoRelationData scope
            $scope.UmbracoRelationData = data;

            //Get Product Data from api
            $http({ method: "Get", url: "Api/Catalogue/GetAllProducts" })
                .success(function (data) {
                    def2.resolve(data);

                })
                .error(function () {
                    $scope.error = "Property editor failed to load";
                })

        });

        def2.promise.then(function (data) {
            //Add api data to Product scope
            for (var x = 0; x < data.length; x++) {
                $scope.Products.push({ Info: data[x], Selected: false });
            }

            $scope.ProductName = data.Name;

            //Keeps check box choices checked after save and publish
            updatePropertyView($scope.Products, $scope.UmbracoRelationData);
        });


        //Get Relations Data from api
        $http({ method: "Get", url: "Api/Catalogue/GetAllUmbracoRelations" })
            .success(function (data) {
                def1.resolve(data);
            })
            .error(function () {
                $scope.error = "Property editor failed to load";
             })

        //Listen for any changes to the scope i.e. changes in the checked state of checkboxes
        $scope.$watch("Products", function (newVal, oldVal) {
            $scope.model.value = new Array();

            for (var x = 0; x < $scope.Products.length; x++) {
                    $scope.model.value.push({ ProductId: $scope.Products[x].Info.ProductID, Selected: $scope.Products[x].Selected })
            }


        }, true);

        //Keeps check box choices checked after save and publish
        function updatePropertyView(Products, UmbracoRelationData) {

            for (var x = 0; x < UmbracoRelationData.length; x++) {
                //Test if current product is linked to current category
                if (UmbracoRelationData[x].ChildId == editorState.current.id) {
                    var index = findIndex(Products, UmbracoRelationData[x].ParentId);
                    if (index != -1) {
                        $scope.Products[index].Selected = true;
                    }
                }
            }
        }

        //Returns the index of an array
        function findIndex(arr, item) {
            var index = -1;
            for (var x = 0; x < arr.length; x++) {
                if (arr[x].Info.ProductID == item) {
                        index = x;
                        return index;
                }
            }

            return index;

        }

    });







