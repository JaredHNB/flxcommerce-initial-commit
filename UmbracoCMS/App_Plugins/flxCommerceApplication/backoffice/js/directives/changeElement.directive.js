﻿angular.module("umbraco").directive('changeElement', function ($compile, flxCampaignsResource) {
    return {
        restrict: 'AEC',
        link: function (scope, element, attrs, controllers) {
            element.on('mouseover', function () {
                var tag = element[0].tagName;
                var property = element.attr('ng-model');
                if (tag == "DIV") {
                    var newElement = $compile('<input class="col-4" type="text" value="' + element[0].innerText + '"ng-model="' + property + '" change-element/>')(scope);

                    element.replaceWith(newElement);
                    element = newElement;
                }
            });

            element.on('mouseleave', function () {
                var tag = element[0].tagName;
                var property = element.attr('ng-model');

                scope.$watch('campaign', function (newVal, oldVal) {
                        flxCampaignsResource.updateMarketingCampaign(scope.campaign);
                });

                if (tag == "INPUT") {
                    var newElement = $compile('<div class="col-4" ng-model="' + property + '" change-element>' + element[0].value + '</div>')(scope);

                    element.replaceWith(newElement);
                    element = newElement;

                }
            });
        }

    };
});