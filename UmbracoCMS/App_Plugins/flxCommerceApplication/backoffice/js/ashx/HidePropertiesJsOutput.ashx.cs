﻿using System.Configuration;
using System.Web;

namespace Control2K_Website.App_Plugins.flxCommerceApplication.backoffice.js.ashx
{
    /// <summary>
    /// Allows hiding of fields in Umbraco Admin tools
    /// Editable via the HideBackEndFields appkey in web.config
    /// Expects a comma seperated string in web.config containing all aliases of fields to be hidden
    /// </summary>
    public class HidePropertiesJsOutput : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/javascript";
            string fieldsToHide = ConfigurationManager.AppSettings.Get("HideBackEndFields");
            if (!string.IsNullOrEmpty(fieldsToHide))
            {
                foreach (string field in fieldsToHide.Split(','))
                {
                    //Original reponse - doesnt work as not all fields get the "data-element" property added to them.
                    //context.Response.Write("$(document).arrive(\"div[data-element='property-" + field + "']\", function () { $(this).hide(); });\r\n");

                    //Tom 19.10.2018 version looks for the label tag instead (which uses the alias) then goes up two levels and hides that.
                    context.Response.Write("$(document).arrive(\"label[title='" + field + "']\", function () { $(this).parent().parent().hide(); });\r\n");
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}