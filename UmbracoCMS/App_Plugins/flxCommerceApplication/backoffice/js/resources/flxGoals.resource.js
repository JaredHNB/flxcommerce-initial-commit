﻿angular.module("umbraco").factory("flxGoalsResource", function ($http) {
    return {

        getAllGoalsByPromotionId: function (promotionId) {
            return $http.get("/api/PromotionWebService/GetAllGoalsByPromotionId?promotionId=" + promotionId)
        },
        getGoalById: function (goalId) {
            return $http.get("/api/PromotionWebService/GetGoalById?goalId=" + goalId)
        },
        createGoal: function (goal) {
            return $http.post("/api/PromotionWebService/CreateGoal", goal)
        },
        updatePromotionGoal: function (goal) {
            return $http.post("/api/PromotionWebService/UpdatePromotionGoal", goal)
        },
        deletePromotionGoal: function (goal) {
            return $http.post("/api/PromotionWebService/DeletePromotionGoal", goal)
        },
        getAllRoles: function () {
            return $http.get("/api/CustomerWebService/GetRoles")
        },
    }
});
        
