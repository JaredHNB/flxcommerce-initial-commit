﻿angular.module("umbraco").factory("flxRewardsResource", function ($http) {
    return {

        getAllRewardsByPromotionId: function (promotionId) {
            return $http.get("/api/PromotionWebService/GetAllRewardsByPromotionId?promotionId=" + promotionId)
        },
        getRewardById: function (rewardId) {
            return $http.get("/api/PromotionWebService/GetRewardById?rewardId=" + rewardId)
        },
        createReward: function (reward) {
            return $http.post("/api/PromotionWebService/CreateReward", reward)
        },
        updatePromotionReward: function (reward) {
            return $http.post("/api/PromotionWebService/UpdatePromotionReward", reward)
        },
        deletePromotionReward: function (reward) {
            return $http.post("/api/PromotionWebService/DeletePromotionReward", reward)
        },
    }
});
        
