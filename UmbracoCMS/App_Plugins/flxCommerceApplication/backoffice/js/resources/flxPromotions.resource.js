﻿angular.module("umbraco").factory("flxPromotionsResource", function ($http) {
    return {

        getAllPromotionsByMarketingCampaignId: function (marketingCampaignId) {
            return $http.get("/api/PromotionWebService/GetAllPromotionsByMarketingCampaignId?marketingCampaignId=" + marketingCampaignId)
        },
        getAllPromotionsByMarketingCampaignId: function (marketingCampaignId) {
            return $http.get("/api/PromotionWebService/GetAllPromotionsByMarketingCampaignId?marketingCampaignId=" + marketingCampaignId)
        },
        createPromotion: function (promotion) {
            return $http.post("/api/PromotionWebService/CreatePromotion", promotion)
        },
        updatePromotion: function (promotion) {
            return $http.post("/api/PromotionWebService/UpdatePromotion", promotion)
        },
        deletePromotion: function (promotion) {
            return $http.post("/api/PromotionWebService/DeletePromotion", promotion)
        },
    }
});
        
