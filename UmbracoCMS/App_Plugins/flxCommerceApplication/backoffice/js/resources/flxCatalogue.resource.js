﻿angular.module("umbraco").factory("flxCatalogueResource", function ($http) {
    return {

        getProductById: function (productId) {
            return $http.get("/Api/Catalogue/GetProductById?productId=" + productId)
        },
        getAllProducts: function () {
            return $http.get("/Api/Catalogue/GetAllProducts")
        }
    }
});
        
