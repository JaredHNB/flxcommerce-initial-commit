﻿var umb = angular.module("umbraco.resources").factory("flxSubscribersResource", function ($http) {
    return {

        getAllSubscribers: function () {
            return $http.get("/api/NewsletterWebService/GetAllSubscribers")
        },
        createSubscriberCSV: function (subscribers) {
            return $http.post("/api/NewsletterWebService/CreateSubscriberCSV", subscribers)
        }
    }
    
});

