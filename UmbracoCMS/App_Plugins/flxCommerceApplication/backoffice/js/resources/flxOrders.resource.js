﻿var umb = angular.module("umbraco.resources").factory("flxOrdersResource", function ($http) {
    return {

        getAllOrders: function () {
            return $http.get("/api/OrderWebService/GetAllOrders")
        },
        getAwaitingDispatch: function () {
            var json = { orderStatus: 'Awaiting Dispatch' };
            return $http({
                method: 'POST',
                url: "/api/OrderWebService/GetAllOrders", data: json
            })
        },
        getOrdersOfSpecificStatus: function (data) {
            var json = { orderStatus: data };
            return $http({
                method: 'POST',
                url: "/api/OrderWebService/GetAllOrders", data: json
            })
        },
        getOrdersByDate: function (data) {
            var json = {
                orderStatusIds: data.orderStatusIds,
                startDate: data.startDate,
                endDate: data.endDate
            };
            return $http({
                method: 'POST',
                url: "/api/OrderWebService/GetOrdersByDate", data: json
            })
        },
        getShippedOrders: function () {
            var json = { orderStatus: 'Shipped' };
            return $http({
                method: 'POST',
                url: "/api/OrderWebService/GetAllOrders", data: json
            })
        },
        getCancelledOrders: function () {
            var json = { orderStatus: 'Cancelled' };
            return $http({
                method: 'POST',
                url: "/api/OrderWebService/GetAllOrders", data: json
            })
        },
        getOrderById: function (orderId) {
            return $http.get("/api/OrderWebService/GetOrderById?orderId=" + orderId)
        },
        getAllOrderStatus: function () {
            return $http.get("/api/OrderWebService/GetAllOrderStatus")
        },
        createReportCSV: function (orderIds) {
            var json = { orders: orderIds };
            return $http.post("/api/ReportingWebService/CreateOrderReport", json)
        },
        getOrderTotalsForLastSevenDays: function (data) {
            var json = {
                orderStatusIds: data,
            }
            return $http({
                method: 'POST',
                url:("/api/AnalyticsWebService/GetOrderTotalsForLastSevenDays")
            })
        },
        getNumberOfOrdersForLastSevenDays: function (data) {
            var json = {
                orderStatusIds: data,
            }
            return $http({
                method: 'POST',
                url: "/api/AnalyticsWebService/GetNumberOfOrdersInLastSevenDays", data: json
            })
        },
        getOrderTotalsForDateRange: function (data) {
            var json = {
                orderStatusIds: data.orderStatusIds,
                startDate: data.startDate,
                endDate: data.endDate

            }
            return $http({
                method: 'POST',
                url: "/api/AnalyticsWebService/GetOrderTotalsForDateRange", data: json
            })
        },
        getTotalOrdersByStatus: function (data) {
            var json = {
                orderStatusId: data,
            }
            return $http({
                method: 'POST',
                url: "/api/AnalyticsWebService/GetTotalOrdersByStatus", data: json
            })
        }


        

    }
});
        
