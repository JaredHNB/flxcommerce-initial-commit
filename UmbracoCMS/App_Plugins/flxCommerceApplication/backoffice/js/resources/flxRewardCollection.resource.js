﻿angular.module("umbraco").factory("flxRewardCollection", function () {

    var rewardCollection = new Array();
    rewardCollection.push({ icon: 'cube', name: 'Free product', templateName: 'addFreeProduct' });
    rewardCollection.push({ icon: 'money', name: 'Percent off order', templateName: 'addPercentOffOrder' });
    return {

        getCollection: function () {
            return rewardCollection
        },

    };
});
        
