﻿angular.module("umbraco").factory("flxToolsResource", function () {

    var templates = new Array();
    //Dashboards
    templates["promotion"] = "/App_Plugins/flxCommerceApplication/backoffice/flxPromotion/dashboard.html";
    templates["orders"] = "/App_Plugins/flxCommerceApplication/backoffice/flxOrders/views/dashboard.html";
    templates["settings"] = "/App_Plugins/flxCommerceApplication/backoffice/flxSettings/dashboard.html";
    templates["flxCatalogue"] = "/App_Plugins/flxCommerceApplication/backoffice/flxCatalogue/views/dashboard.html";

    //Promotion Views
    templates["promotion-activeCampaigns"] = "/App_Plugins/flxCommerceApplication/backoffice/flxPromotion/campaigns/views/activeCampaigns.html";
    templates["promotion-expiredCampaigns"] = "/App_Plugins/flxCommerceApplication/backoffice/flxPromotion/campaigns/views/expiredCampaigns.html";
    templates["newsletter-subscribers"] = "/App_Plugins/flxCommerceApplication/backoffice/flxPromotion/newsletters/views/Subscribers.html";

    //Order Views Without Date Range
    templates["orders-Paid"] = "/App_Plugins/flxCommerceApplication/backoffice/flxOrders/views/default.html";
    templates["orders-For Review"] = "/App_Plugins/flxCommerceApplication/backoffice/flxOrders/views/default.html";
    templates["orders-Processing"] = "/App_Plugins/flxCommerceApplication/backoffice/flxOrders/views/default.html";
    templates["orders-Awaiting Payment"] = "/App_Plugins/flxCommerceApplication/backoffice/flxOrders/views/default.html";

    //Order Views With Date Range
    templates["orders-Basket"] = "/App_Plugins/flxCommerceApplication/backoffice/flxOrders/views/defaultWithDate.html";
    templates["orders-Completed"] = "/App_Plugins/flxCommerceApplication/backoffice/flxOrders/views/defaultWithDate.html";
    templates["orders-Cancelled"] = "/App_Plugins/flxCommerceApplication/backoffice/flxOrders/views/defaultWithDate.html";
    templates["orders-All"] = "/App_Plugins/flxCommerceApplication/backoffice/flxOrders/views/defaultWithDate.html";
   
    //Order Print Template
    templates["orders-invoicePrintPage"] = "/App_Plugins/flxCommerceApplication/backoffice/flxOrders/views/invoicePrintPage.html";
    
    return {

        getTemplate: function (template) {


            return templates[template];
        },

    };
});

