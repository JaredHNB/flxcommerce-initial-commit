﻿angular.module("umbraco").factory("flxGoalCollection", function () {

    var goalCollection = new Array();
    goalCollection.push({ icon: 'ticket', name: 'Coupon Code',  templateName: 'addCouponCode' });
    goalCollection.push({ icon: 'money', name: 'Spend more than', templateName: 'addSpendMoreThan' });
    goalCollection.push({ icon: 'user', name: 'Is Of Role', templateName: 'addIsOfRole' });
    goalCollection.push({ icon: 'user', name: 'Is Not Of Role', templateName: 'addIsNotOfRole' });
    return {

        getCollection: function () {
            return goalCollection
        },

    };
});
        
