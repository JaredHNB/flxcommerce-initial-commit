﻿angular.module("umbraco").factory("flxCampaignsResource", function ($http) {
    return {

        getAllMaketingCampaigns: function () {
            return $http.get("/api/PromotionWebService/GetAllMarketingCampaigns")
        },
        getAllCurrentMarketingCampaigns: function () {
            return $http.get("/api/PromotionWebService/GetAllCurrentMarketingCampaigns")
        },
        getMarketingCampaignById: function (marketingCampaignId) {
            return $http.get("/api/PromotionWebService/GetMarketingCampaignById?marketingCampaignId=" + marketingCampaignId)
        },
        updateMarketingCampaign: function (marketingCampaign) {
            return $http.post("/api/PromotionWebService/UpdateMarketingCampaign", marketingCampaign)
        },
        createMarketingCampaign: function (marketingCampaign) {
            return $http.post("/api/PromotionWebService/CreateMarketingCampaign", marketingCampaign)
        },
        deleteMarketingCampaign: function (marketingCampaign) {
            return $http.post("/api/PromotionWebService/DeleteMarketingCampaign", marketingCampaign)
        }

    }
});
        
