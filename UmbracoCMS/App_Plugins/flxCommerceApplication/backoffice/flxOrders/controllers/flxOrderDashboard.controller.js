﻿
angular.module("umbraco").controller("flxOrderDashboard.Controller",
   function ($scope, flxOrdersResource,  $filter, $http, filterFilter, $window) {


       //Initialise Order array
       $scope.OrderList = {};
       $scope.OrderStatuses = {};
       //Set loading graphic to on
       $scope.loading = true;
       $scope.noteloading = false;
       $scope.dispatching = false;
       $scope.updateStatusButton = false;
       $scope.sortType = 'OrderDate'; // set the default sort type
       $scope.sortReverse = true;  // set the default sort order
       $scope.numberofselectedorders = {};
       $scope.s = '';
       $scope.selectedItem = null;
       $scope.SideTemplates = new Array();
       $scope.SideTemplates["1"] = "/App_Plugins/flxCommerceApplication/backoffice/flxOrders/orderNotesViewPage.html";
       $scope.SideTemplates["2"] = "/App_Plugins/flxCommerceApplication/backoffice/flxOrders/orderViewPage.html";
       $scope.sideBarOrder = {};
       //Note Sidebar Variables
       $scope.newNote = { note: '', Selected: false };
       $scope.OrderList.OrderStatuses = {};
       $scope.downloadLink = false;
       $scope.url = "";
       $scope.weeklyIncrease = 0;
       $scope.page = null;
       $scope.emptyList = false;
       //Days of the week for Charts
       var weekday = new Array(7);
       weekday[0] = "Sunday";
       weekday[1] = "Monday";
       weekday[2] = "Tuesday";
       weekday[3] = "Wednesday";
       weekday[4] = "Thursday";
       weekday[5] = "Friday";
       weekday[6] = "Saturday";
       weekday[7] = "Sunday";
       weekday[8] = "Monday";
       weekday[9] = "Tuesday";
       weekday[10] = "Wednesday";
       weekday[11] = "Thursday";
       weekday[12] = "Friday";
       weekday[13] = "Saturday";
       weekday[14] = "Sunday";
     

       $scope.shipped = 0;
       $scope.awaitingPrint = 0;
       $scope.cancelled = 0;


      
       //Page Order Status
       $scope.page = angular.element(document.getElementsByName('orderstatus')[0]).val();
       //Set Default days
       $scope.date = {};
       var now = new Date();
       $scope.date.ends = $filter("date")(now, 'yyyy-MM-ddTHH:mm');
       now.setDate(now.getDate() - 7);
       $scope.date.starts = $filter("date")(now, 'yyyy-MM-ddTHH:mm');

      
       $scope.lastSevenDaysOrderTotal = 0;
       $scope.lastSevenDaysNumberOfOrders = 0;
       $scope.previousSevenDaysOrderTotal = 0;
       $scope.previousSevenDaysNumberOfOrders = 0;

       $scope.orderTotalPercentageChange = 0;
       $scope.orderNumberPercentageChange = 0;

       //fill awaiting dispatch
           flxOrdersResource.getOrdersOfSpecificStatus("Awaiting Dispatch").then(function (response) {
               $scope.OrderList.AwaitingDispatch = response.data;
               if ($scope.OrderList.AwaitingDispatch.length == 0) {
                   $scope.emptyList = true;
               }
               $scope.OrderList.AwaitingDispatch.Total = 0;
               angular.forEach($scope.OrderList.AwaitingDispatch, function (value, key) {
                   $scope.OrderList.AwaitingDispatch.Total += value.OrderTotal;

               });
               
               //once complete set loading graphic to off
               $scope.loading = false;
            
           });

       /**fill awaiting Print
           flxOrdersResource.getTotalOrdersByStatus("8").then(function (response) {
               $scope.awaitingPrint = response.data;
           });

       //fill shipped
           flxOrdersResource.getTotalOrdersByStatus("4").then(function (response) {
               $scope.shipped = response.data;
           });

       //fill cancelled
           flxOrdersResource.getTotalOrdersByStatus("5").then(function (response) {
               $scope.cancelled = response.data;
           });

           */
    
       $scope.chart = function () {
           var today = new Date().getDay();
           var d = today+7;

           //Set Data
           var orderStatusIds = [];
           orderStatusIds.push("2");
           orderStatusIds.push("3");
           orderStatusIds.push("4");
           orderStatusIds.push("5");
           var data = {};
           data.orderStatusIds = [];
           data.orderStatusIds = orderStatusIds;
           var now = new Date();
           data.endDate = now;

           var fourteenDaysAgo = now - 1000 * 60 * 60 * 24 * 13; 
           
           
           data.startDate = new Date(fourteenDaysAgo);
           flxOrdersResource.getOrderTotalsForDateRange(data)
             .then(function (response) {

                 var test = response.data;
                 var previousSevenDays = response.data.slice(0, 7);
                 var lastSevenDays = response.data.slice(7,14);
                 $scope.lastSevenDaysNumberOfOrders = lastSevenDays[0].numberOfOrders + lastSevenDays[1].numberOfOrders + lastSevenDays[2].numberOfOrders + lastSevenDays[3].numberOfOrders + lastSevenDays[4].numberOfOrders + lastSevenDays[5].numberOfOrders + lastSevenDays[6].numberOfOrders;
                 $scope.lastSevenDaysOrderTotal = lastSevenDays[0].ordersTotal + lastSevenDays[1].ordersTotal + lastSevenDays[2].ordersTotal + lastSevenDays[3].ordersTotal + lastSevenDays[4].ordersTotal + lastSevenDays[5].ordersTotal + lastSevenDays[6].ordersTotal;

                 $scope.previousDaysNumberOfOrders = previousSevenDays[0].numberOfOrders + previousSevenDays[1].numberOfOrders + previousSevenDays[2].numberOfOrders + previousSevenDays[3].numberOfOrders + previousSevenDays[4].numberOfOrders + previousSevenDays[5].numberOfOrders + previousSevenDays[6].numberOfOrders;
                 $scope.previousSevenDaysOrderTotal = previousSevenDays[0].ordersTotal + previousSevenDays[1].ordersTotal + previousSevenDays[2].ordersTotal + previousSevenDays[3].ordersTotal + previousSevenDays[4].ordersTotal + previousSevenDays[5].ordersTotal + previousSevenDays[6].ordersTotal;

                 $scope.orderTotalPercentageChange = $scope.calcPercentage($scope.lastSevenDaysOrderTotal, $scope.previousSevenDaysOrderTotal);
                 $scope.orderNumberPercentageChange = $scope.calcPercentage($scope.lastSevenDaysNumberOfOrders, $scope.previousDaysNumberOfOrders);
                 
           //Set Labels
           var labels = [(weekday[d - 6]), weekday[d - 5], weekday[d - 4], weekday[d - 3], weekday[d - 2], weekday[d - 1], weekday[d]];
          
           var lineChartData = {
               labels: labels,
               datasets: [
                   
                   {
                       label: "Previous Week",
                       fillColor: "rgba(220,220,220,0.2)",
                       strokeColor: "rgba(220,220,220,1)",
                       pointColor: "rgba(220,220,220,1)",
                       pointStrokeColor: "#fff",
                       pointHighlightFill: "#fff",
                       pointHighlightStroke: "rgba(220,220,220,1)",
                       data: [previousSevenDays[0].ordersTotal, previousSevenDays[1].ordersTotal, previousSevenDays[2].ordersTotal, previousSevenDays[3].ordersTotal, previousSevenDays[4].ordersTotal, previousSevenDays[5].ordersTotal, previousSevenDays[6].ordersTotal]
                   },
                   {
                       label: "This Week",
                       fillColor: "rgba(151,187,205,0.2)",
                       strokeColor: "rgba(151,187,205,1)",
                       pointColor: "rgba(151,187,205,1)",
                       pointStrokeColor: "#fff",
                       pointHighlightFill: "#fff",
                       pointHighlightStroke: "rgba(151,187,205,1)",
                       data: [lastSevenDays[0].ordersTotal, lastSevenDays[1].ordersTotal, lastSevenDays[2].ordersTotal, lastSevenDays[3].ordersTotal, lastSevenDays[4].ordersTotal, lastSevenDays[5].ordersTotal, lastSevenDays[6].ordersTotal]
                   }

               ]

           }

           var barChartData = {
               labels: labels,
               datasets: [

                   {
                       label: "Previous Week",                       
                       fillColor: "rgba(220,220,220,0.5)",
                       strokeColor: "rgba(220,220,220,0.8)",
                       highlightFill: "rgba(220,220,220,0.75)",
                       highlightStroke: "rgba(220,220,220,1)",
                       data: [previousSevenDays[0].numberOfOrders, previousSevenDays[1].numberOfOrders, previousSevenDays[2].numberOfOrders, previousSevenDays[3].numberOfOrders, previousSevenDays[4].numberOfOrders, previousSevenDays[5].numberOfOrders, previousSevenDays[6].numberOfOrders]
                   },
                   {
                       label: "This Week",
                       fillColor: "rgba(151,187,205,0.5)",
                       strokeColor: "rgba(151,187,205,0.8)",
                       highlightFill: "rgba(151,187,205,0.75)",
                       highlightStroke: "rgba(151,187,205,1)",
                       data: [lastSevenDays[0].numberOfOrders, lastSevenDays[1].numberOfOrders, lastSevenDays[2].numberOfOrders, lastSevenDays[3].numberOfOrders, lastSevenDays[4].numberOfOrders, lastSevenDays[5].numberOfOrders, lastSevenDays[6].numberOfOrders]
                   }
               ]

           }

           var ctx = document.getElementById("canvasTwo").getContext("2d");
           ctx.canvas.width = 300;
           ctx.canvas.height = 300;
           var myLineChart = new Chart(ctx).Bar(barChartData, {
               responsive: true, maintainAspectRatio: false
           });

           var ctx = document.getElementById("canvasOne").getContext("2d");
           ctx.canvas.width = 300;
           ctx.canvas.height = 300;
           var myLineChart = new Chart(ctx).Line(lineChartData, {
               responsive: true, maintainAspectRatio: false, scaleLabel: "<%='£'+value%>", multiTooltipTemplate: "<%= datasetLabel %> - <%='£'+value%>"
           });
             });

       };

       $scope.calcPercentage = function (a, b) {

           return ((a - b) / b) * 100;
       }

                    

       //Populate Charts
       $scope.chart();
      
  



       // Set the initial X/Y values.
       $scope.mouseX = "N/A";
       $scope.mouseY = "N/A";

       $(document).click(function (event) {

           $scope.mouseX = event.pageX;
           $scope.mouseY = event.pageY;
           $scope.$apply();

       });


   });

angular.module("umbraco").directive('ngHover', function () {
    return function (scope, element) {
        element.bind('mouseenter', function () {
            element.addClass('hoverover');
        }).bind('mouseleave', function () {
            element.removeClass('hoverover');
        })
    }
});

angular.module("umbraco").directive('ngHoverGrey', function () {
    return function (scope, element) {
        element.bind('mouseenter', function () {
            element.removeClass('greyedout');
        }).bind('mouseleave', function () {
            element.addClass('greyedout');
        })
    }
});

angular.module("umbraco").directive('initData', function ($parse) {
    return function (scope, element, attrs) {
        //modify scope
        var model = $parse(attrs.initData);
        model(scope);
    };
});
angular.module("umbraco").filter('removechars', function () {
    return function (text, chars) {
        for (var i = 0; i < chars.length; i++) {
            text = text.replace(chars[i], "");
        }
        return text;
    };
})




