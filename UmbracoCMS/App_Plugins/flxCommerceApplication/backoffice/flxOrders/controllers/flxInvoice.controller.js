﻿angular.module("umbraco").controller("flxInvoice.Controller",
   function ($scope, $window) {
       
        //Initialise array from passed in data
       $scope.invoices = $window.mySharedData;

       //Prints page
       $scope.printPage = function () {
           $window.print();
       };

       //Closes page
       $scope.closePage = function () {
           $window.close();
       };
       

   });


angular.module("umbraco").filter('clickncollect', function () {
    return function (text) {
        return text.replace("( See below for location details)", "");
    };
})