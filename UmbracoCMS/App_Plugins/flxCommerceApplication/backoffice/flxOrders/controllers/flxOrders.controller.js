﻿
angular.module("umbraco").controller("flxOrders.Controller",
   function ($scope, flxOrdersResource, $filter, $http, filterFilter, $window, $location) {
       //Page Order Status
       $scope.page = $location.path().split('orders-')[1];

      
       //Initialise Order array
       $scope.OrderList = {};
       $scope.OrderStatuses = {};
       //Set loading graphic to on
       $scope.loading = true;
       $scope.noteloading = false;
       $scope.dispatching = false;
       $scope.updateStatusButton = false;
       $scope.sortType = 'OrderDate'; // set the default sort type
       $scope.sortReverse = true;  // set the default sort order
       $scope.numberofselectedorders = {};
       $scope.s = '';
       $scope.selectedItem = null;
       $scope.SideTemplates = new Array();       
       $scope.SideTemplates["1"] = "/App_Plugins/flxCommerceApplication/backoffice/flxOrders/views/orderNotesViewPage.html";
       $scope.SideTemplates["2"] = "/App_Plugins/flxCommerceApplication/backoffice/flxOrders/views/orderViewPage.html";
       $scope.sideBarOrder = {};
       //Note Sidebar Variables
       $scope.newNote = { note: '', Selected: false };
       $scope.OrderList.OrderStatuses = {};
       $scope.downloadLink = false;
       $scope.url = "";
       

     
       $scope.emptyList = false;

     
       //Set Default days
       $scope.date = {};
       var now = new Date();
       $scope.date.ends = $filter("date")(now, 'yyyy-MM-ddTHH:mm');
       now.setDate(now.getDate() - 7);
       $scope.date.starts = $filter("date")(now, 'yyyy-MM-ddTHH:mm');
     
       //Get all orders of the status
       if ($scope.page == "Shipped" || $scope.page == "Cancelled" || $scope.page == "Basket" || $scope.page == "All") {

           var data = {};
           data.orderStatusIds = [];
           if ($scope.page == "Shipped") {
               data.orderStatusIds.push("4");
           }
           if ($scope.page == "Cancelled") {
               data.orderStatusIds.push("5");
           }
           if ($scope.page == "Basket") {
               data.orderStatusIds.push("1");
           }

           if ($scope.page == "All") {
               data.orderStatusIds.push("3");
               data.orderStatusIds.push("4");
               data.orderStatusIds.push("5");
           }

           data.startDate = $scope.date.starts;
           data.endDate = $scope.date.ends;


           flxOrdersResource.getOrdersByDate(data).then(function (response) {
               $scope.OrderList = response.data;
               if ($scope.OrderList.length == 0)
               {
                   $scope.emptyList = true;
               }
               //once complete set loading graphic to off
               $scope.loading = false;
           });

       } else {
           flxOrdersResource.getOrdersOfSpecificStatus($scope.page).then(function (response) {
               $scope.OrderList = response.data;
               if ($scope.OrderList.length == 0) {
                   $scope.emptyList = true;
               }
               //once complete set loading graphic to off
               $scope.loading = false;
           });
       }

    //Populate OrderStatus array
    flxOrdersResource.getAllOrderStatus()
           .then(function (response) {
               $scope.OrderStatuses = response.data;
               
           });

       //Updates Orders list to new date range
    $scope.updateOrderList = function () {
        $scope.loading = true;
        $scope.emptyList = false;
        var data = {};
        data.orderStatusIds = [];
        if ($scope.page == "Shipped") {
            data.orderStatusIds.push("4");
        }
        if ($scope.page == "Cancelled") {
            data.orderStatusIds.push("5");
        }
        if ($scope.page == "Basket") {
            data.orderStatusIds.push("1");
        }

        if ($scope.page == "All") {
            data.orderStatusIds.push("3");
            data.orderStatusIds.push("4");
            data.orderStatusIds.push("5");
        }
        data.startDate = $scope.date.starts;
        data.endDate = $scope.date.ends;


        flxOrdersResource.getOrdersByDate(data).then(function (response) {
            $scope.OrderList = response.data;
            if ($scope.OrderList.length == 0) {
                $scope.emptyList = true;
            }
            //once complete set loading graphic to off
            $scope.loading = false;
        });
    };

    
    //Opens print node for multiple orders
    $scope.printOrders = function () {
        var popupWindow = $window.open("#/flxCommerce/flxTools/landing/orders-invoicePrintPage");
        //populate shared data variable with the selected orders
        popupWindow.mySharedData = $scope.selectedOrders();
    };

    //Opens print node for single order
    $scope.printOrder = function (orderID) {
        var popupWindow = $window.open("#/flxCommerce/flxTools/landing/orders-invoicePrintPage");
        //populate shared data variable with the selected orders
        var orders = $filter('filter')($scope.OrderList, orderID, true);
        popupWindow.mySharedData = orders;
    };
       

    $scope.printPage = function () {
        $window.print();
    };

    //Sets a given order to a given status
    $scope.updateOrderStatus = function (orderStatusID, orderID) {
        var confirmation = $window.confirm("Are you sure?");
        var data = {};
        data.orderID = orderID;
        data.orderStatusID = orderStatusID;
        $scope.dispatching = true;

        if (confirmation == true) {
        $http({
            method: 'POST',
            url: '/api/OrderWebService/UpdateOrderStatus',
            data: data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
        }).success(function (data, status, headers, config) {
            //Fetch the orders again and repopulate the scope

            //Get all orders of the status
            if ($scope.page == "Shipped" || $scope.page == "Cancelled" || $scope.page == "Basket") {

                var data = {};
                data.orderStatusIds = [];
                if ($scope.page == "Shipped") {
                    data.orderStatusIds.push("4");
                }
                if ($scope.page == "Cancelled") {
                    data.orderStatusIds.push("5");
                }
                if ($scope.page == "Basket") {
                    data.orderStatusIds.push("1");
                }

                if ($scope.page == "All") {
                    data.orderStatusIds.push("3");
                    data.orderStatusIds.push("4");
                    data.orderStatusIds.push("5");
                }
                data.startDate = $scope.date.starts;
                data.endDate = $scope.date.ends;


                flxOrdersResource.getOrdersByDate(data).then(function (response) {
                    $scope.OrderList = response.data;
                    if ($scope.OrderList.length == 0) {
                        $scope.emptyList = true;
                    }
                    //once complete set loading graphic to off
                    $scope.dispatching = false;

                });

            } else {
                flxOrdersResource.getOrdersOfSpecificStatus($scope.page).then(function (response) {
                    $scope.OrderList = response.data;
                    if ($scope.OrderList.length == 0) {
                        $scope.emptyList = true;
                    }
                    //once complete set loading graphic to off
                    $scope.dispatching = false;

                });
            }
        });
    } else {
        $scope.dispatching = false;
    }

    };



    $scope.updateButtons = function () {
        $scope.numberofselectedorders = $scope.selectedOrders();
        if ($scope.numberofselectedorders.length > 1) {
            $scope.s = 's';
        } else {
            $scope.s = '';
        }

    };

    //Set Sidebar to template    
    $scope.selectsidetemplate = function (index, orderID) {
        if (index == 0) {
            $scope.SideTemplate = null;
        }

        if ($scope.SideTemplates[index] != null) {
            $scope.sideBarOrder = $filter('filter')($scope.OrderList, orderID, true)[0];
            $scope.SideTemplate = $scope.SideTemplates[index];
        }
        else {
            $scope.SideTemplate = null;
        }
    };
              
    //Call Api and dispatch the Orders selected
   	$scope.dispatchOrders = function () {
        //Get the selected orders
   	    var ordersForDispatch = $scope.selectedOrders();
   	    var confirmation = $window.confirm("Have you printed these orders?");
   	    $scope.dispatching = true;
   	    if (confirmation == true) {

   	        $http({
   	            method: 'POST',
   	            url: '/api/OrderWebService/DispatchOrders',
   	            data: ordersForDispatch,
   	            contentType: "application/json; charset=utf-8",
   	            dataType: "json",
   	        }).success(function (data, status, headers, config) {
   	            //Fetch the orders again and repopulate the scope
   	            //Get all orders of the status
   	            if ($scope.page == "Shipped" || $scope.page == "Cancelled" || $scope.page == "Basket") {

   	                var data = {};
   	                data.orderStatusIds = [];
   	                if ($scope.page == "Shipped") {
   	                    data.orderStatusIds.push("4");
   	                }
   	                if ($scope.page == "Cancelled") {
   	                    data.orderStatusIds.push("5");
   	                }
   	                if ($scope.page == "Basket") {
   	                    data.orderStatusIds.push("1");
   	                }

   	                if ($scope.page == "All") {
   	                    data.orderStatusIds.push("3");
   	                    data.orderStatusIds.push("4");
   	                    data.orderStatusIds.push("5");
   	                }
   	                data.startDate = $scope.date.starts;
   	                data.endDate = $scope.date.ends;


   	                flxOrdersResource.getOrdersByDate(data).then(function (response) {
   	                    $scope.OrderList = response.data;
   	                    if ($scope.OrderList.length == 0) {
   	                        $scope.emptyList = true;
   	                    }
   	                    //once complete set loading graphic to off
   	                    $scope.dispatching = false;

   	                });

   	            } else {
   	                flxOrdersResource.getOrdersOfSpecificStatus($scope.page).then(function (response) {
   	                    $scope.OrderList = response.data;
   	                    if ($scope.OrderList.length == 0) {
   	                        $scope.emptyList = true;
   	                    }
   	                    //once complete set loading graphic to off
   	                    $scope.dispatching = false;

   	                });
   	            }
   	        });
   	    } else {
   	        $scope.dispatching = false;
   	    }
   	};//end dispatchOrders

    //Call Api and dispatch the Order passed in
   	$scope.dispatchorder = function (order) {   	    
   	    var confirmation = $window.confirm("Have you printed this order?");
   	    $scope.dispatching = true;
   	    if (confirmation == true) {
   	        $http({
   	            method: 'POST',
   	            url: '/api/OrderWebService/DispatchOrder',
   	            data: order,
   	            contentType: "application/json; charset=utf-8",
   	            dataType: "json",
   	        }).success(function (data, status, headers, config) {
   	            //Fetch the orders again and repopulate the scope
   	            //Get all orders of the status
   	            if ($scope.page == "Shipped" || $scope.page == "Cancelled" || $scope.page == "Basket") {

   	                var data = {};
   	                data.orderStatusIds = [];
   	                if ($scope.page == "Shipped") {
   	                    data.orderStatusIds.push("4");
   	                }
   	                if ($scope.page == "Cancelled") {
   	                    data.orderStatusIds.push("5");
   	                }
   	                if ($scope.page == "Basket") {
   	                    data.orderStatusIds.push("1");
   	                }

   	                if ($scope.page == "All") {
   	                    data.orderStatusIds.push("3");
   	                    data.orderStatusIds.push("4");
   	                    data.orderStatusIds.push("5");
   	                }
   	                data.startDate = $scope.date.starts;
   	                data.endDate = $scope.date.ends;


   	                flxOrdersResource.getOrdersByDate(data).then(function (response) {
   	                    $scope.OrderList = response.data;
   	                    if ($scope.OrderList.length == 0) {
   	                        $scope.emptyList = true;
   	                    }
   	                    //once complete set loading graphic to off
   	                    $scope.dispatching = false;
   	                });

   	            } else {
   	                flxOrdersResource.getOrdersOfSpecificStatus($scope.page).then(function (response) {
   	                    $scope.OrderList = response.data;
   	                    if ($scope.OrderList.length == 0) {
   	                        $scope.emptyList = true;
   	                    }
   	                    //once complete set loading graphic to off
   	                    $scope.dispatching = false;
   	                });
   	            }
   	        });
   	    } else {
   	        $scope.dispatching = false;
   	    }
   	};//end dispatchOrder

   	
    //helper method to get selected orders
   	$scope.selectedOrders = function selectedOrders() {
   	    return filterFilter($scope.OrderList, { Selected: true });
   	};

    //check all function for the checkboxes
   	$scope.checkAll = function () {
   	    if ($scope.selectedAll) {
   	        $scope.selectedAll = true;
   	    } else {
   	        $scope.selectedAll = false;
   	    }
   	    angular.forEach($scope.OrderList, function (order) {
   	        order.Selected = $scope.selectedAll;
   	    });
   	    $scope.updateButtons();
   	};//end CheckAll



   //OrderNote sidebar Methods
   	$scope.addNewNote = function () {
   	    $scope.data = {};
   	    $scope.data.note = $scope.newNote.note;
   	    $scope.data.orderID = $scope.sideBarOrder.OrderID;
   	    $scope.data.noteSelected = $scope.newNote.Selected;
   	    $scope.noteloading = true;

   	    $http({
   	        method: 'POST',
   	        url: '/api/OrderWebService/AddNewOrderNote',
   	        data: $scope.data,
   	        contentType: "application/json; charset=utf-8",
   	        dataType: "json",
   	    }).success(function (data, status, headers, config) {

   	        $http({ method: "Get", url: "/api/OrderWebService/GetOrderById?orderId=" + $scope.sideBarOrder.OrderID })
                 .success(function (data) {

                     //set scope to new data
                     $scope.sideBarOrder = data;
                     //re set text box
                     $scope.newNote.note = null;
                     //Un select email tickbox
                     $scope.newNote.Selected = false;
                     $scope.noteloading = false;

                 });
   	    });

   	};//end addNewNote

   	$scope.Generate = function () {
   	    //$scope.orders = {};
   	    $scope.orders = [];
   	    angular.forEach($scope.OrderList, function (order) {
            
   	        $scope.orders.push(order.OrderID);

   	    });
        
   	    flxOrdersResource.createReportCSV($scope.orders).then(function (response) {
   	        $scope.downloadLink = true;
   	        $scope.url = JSON.parse(response.data);

   	    })

   	};//end Generate
       

       // Set the initial X/Y values.
   	$scope.mouseX = "N/A";
   	$scope.mouseY = "N/A";

   	$(document).click(function (event) {

   	    $scope.mouseX = event.pageX;
   	    $scope.mouseY = event.pageY;
   	    $scope.$apply();

   	});


});

angular.module("umbraco").directive('ngHover', function () {
    return function (scope, element) {
        element.bind('mouseenter', function () {
            element.addClass('hoverover');           
        }).bind('mouseleave', function () {
            element.removeClass('hoverover');          
        })
    }
});

angular.module("umbraco").directive('ngHoverGrey', function () {
    return function (scope, element) {
        element.bind('mouseenter', function () {          
            element.removeClass('greyedout');
        }).bind('mouseleave', function () {           
            element.addClass('greyedout');
        })
    }
});

angular.module("umbraco").directive('initData', function ($parse) {
    return function (scope, element, attrs) {
        //modify scope
        var model = $parse(attrs.initData);
        model(scope);
    };
});
angular.module("umbraco").filter('removechars', function () {
    return function (text, chars) {
        for (var i = 0; i < chars.length; i++) {
            text = text.replace(chars[i], "");
        }
        return text;
    };
})




