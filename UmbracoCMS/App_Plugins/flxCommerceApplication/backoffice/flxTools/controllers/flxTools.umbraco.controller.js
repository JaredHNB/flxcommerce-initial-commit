﻿angular.module("umbraco").controller('flxTools.umbraco.controller', function ($scope, $location, flxToolsResource) {


	//Template configuration

	function SelectTemplate(urlPath) {

	    var template = flxToolsResource.getTemplate(urlPath)

	    if (template != null) {
	        $scope.Template = template;
		}
		else {
	        $scope.Template = flxToolsResource.getTemplate('404');
		}
	}

	var urlSegments = $location.path().split('/');

	SelectTemplate(urlSegments[urlSegments.length - 1]);

    // *****Scope variables*****

});