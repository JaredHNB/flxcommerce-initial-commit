﻿angular.module("umbraco").controller("flxNewsletters.Controller",
   function ($scope, $filter, $http, $window, flxSubscribersResource) {

       $scope.loading = true;
       $scope.subscribers = new Array();
       $scope.subscriberBoxList = false;
       $scope.show = "show";
       $scope.generateLink = true;
       $scope.downloadLink = false;

       function GetSubscribers() {
           flxSubscribersResource.getAllSubscribers().then(function (response) {
               if (response.data.length > 0) {
                   $scope.subscribers = response.data;
                   //$scope.noData = false;
                   $scope.loading = false;
               }
               else {
                   //$scope.noData = true;
               }
           }
           )
       };

       GetSubscribers();

       $scope.subscriberBox = function () {

           if ($scope.subscriberBoxList == true) {
               $scope.subscriberBoxList = false;
               $scope.show = "show";
           } else {
               $scope.subscriberBoxList = true;
               $scope.show = "hide";
           }
       }

       $scope.createSubscriberCSV = function () {
           $scope.generateLink = false;

           flxSubscribersResource.createSubscriberCSV($scope.subscribers).then(function (response) {
               $scope.downloadLink = true;

           })
       };



   });