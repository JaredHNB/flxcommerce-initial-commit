﻿angular.module("umbraco").controller('flxRewards.Controller', function ($scope, flxRewardsResource, flxRewardCollection, userService, $filter) {

    // *****Scope variables*****
    $scope.rewardTemplates = new Array();
    $scope.rewardTemplates["rewards"] = "/App_Plugins/flxCommerceApplication/backoffice/flxPromotion/rewards/views/rewards.html";
    $scope.rewardTemplates["rewardCollection"] = "/App_Plugins/flxCommerceApplication/backoffice/flxPromotion/rewards/views/RewardCollection.html";
    $scope.rewardTemplates["addFreeProduct"] = "/App_Plugins/flxCommerceApplication/backoffice/flxPromotion/rewards/views/rewards/addFreeProduct.html";
    $scope.rewardTemplates["editFreeProduct"] = "/App_Plugins/flxCommerceApplication/backoffice/flxPromotion/rewards/views/rewards/editFreeProduct.html";
    $scope.rewardTemplates["addPercentOffOrder"] = "/App_Plugins/flxCommerceApplication/backoffice/flxPromotion/rewards/views/rewards/addPercentOffOrder.html";
    $scope.rewardTemplates["editPercentOffOrder"] = "/App_Plugins/flxCommerceApplication/backoffice/flxPromotion/rewards/views/rewards/editPercentOffOrder.html";

    $scope.rewardTemplate = null;

    $scope.rewards = new Array();
    $scope.reward = null;
    $scope.newReward = null;

    $scope.flxRewardCollection = flxRewardCollection.getCollection();

    $scope.loading = true;

    userService.getCurrentUser().then(function (user) {
        $scope.user = user;
    });

    $scope.SelectRewardTemplate = function (index) {

        if ($scope.rewardTemplates[index] != null) {
            $scope.rewardTemplate = $scope.rewardTemplates[index];
        }
        else {
            $scope.rewardTemplate = null;
        }

    }

    $scope.SelectReward = function (reward, template) {
        $scope.reward = reward;
        $scope.SelectRewardTemplate(template);

    };

    $scope.AddReward = function (newReward, type) {

        newReward.CreatedBy = $scope.user.name;
        newReward.CreatedOn = $filter('date')(Date.now(), 'yyyy-MM-dd HH:mm:ss', null);
        newReward.ModifiedBy = null;
        newReward.ModifiedOn = null;
        newReward.Type = type;
        newReward.PromotionRewardID = 0;
        newReward.PromotionId = $scope.promotion.PromotionId;

        flxRewardsResource.createReward(newReward).then(function (response) {
            $scope.loading = true;
            GetRewardsByPromotionId($scope.promotion.PromotionId);
            $scope.SelectRewardTemplate('rewards');
        })
    };

    $scope.EditReward = function (reward, type) {

        reward.ModifiedBy = $scope.user.name;
        reward.ModifiedOn = $filter('date')(Date.now(), 'yyyy-MM-dd HH:mm:ss', null);
        reward.Type = type;

        flxRewardsResource.updatePromotionReward(reward).then(function () {
            $scope.loading = true;
            GetRewardsByPromotionId($scope.promotion.PromotionId);
            $scope.SelectRewardTemplate('rewards');
        });
    };

    $scope.DeleteReward = function (reward, type) {
        if (confirm('Are you sure you want to delete this promotion?') == true) {
            reward.Type = type;

            flxRewardsResource.deletePromotionReward(reward).then(function () {
                $scope.loading = true;
                GetRewardsByPromotionId($scope.promotion.PromotionId);
                $scope.SelectRewardTemplate('rewards');
            });
        }
    };

    function GetRewardsByPromotionId(promotionId) {
        flxRewardsResource.getAllRewardsByPromotionId(promotionId).then(function (response) {
            if (response.data.length > 0) {
                $scope.rewards = response.data;

                $scope.noData = false;
                $scope.loading = false;
            }
            else {
                $scope.rewards = null;
                $scope.noData = true;
                $scope.loading = false;

            }
        });
    };

    function selectDefaultTemplate() {
        $scope.rewardTemplate = $scope.rewardTemplates["rewards"];
    };

    selectDefaultTemplate();

    $scope.$watch('promotion', function (newVal, oldVal) {
        if ($scope.promotion != null) {
            $scope.loading = true;
            $scope.rewards = null;
            GetRewardsByPromotionId($scope.promotion.PromotionId);

        }
    });

});