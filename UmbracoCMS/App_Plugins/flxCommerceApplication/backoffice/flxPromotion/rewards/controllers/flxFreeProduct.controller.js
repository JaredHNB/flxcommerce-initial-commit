﻿angular.module("umbraco").controller('flxFreeProduct.Controller.Add', function ($scope, flxCatalogueResource) {

    // *****Scope variables*****
    $scope.newReward = {
        Name: null,
        Starts: null,
        Ends: null,
        ProductID: 0,
        Quantity: 0
    };

    $scope.Validate = function (reward, type) {
        if (flxFreeProductValidation(reward)) {
            $scope.AddReward(reward, type)
        }
    }


    function GetAllProducts() {
        flxCatalogueResource.getAllProducts().then(function (response) {
            if (response.data.length > 0) {
                $scope.products = response.data;
                $scope.loading = false;
            }
            else {
                $scope.loading = false;
            }
        });
    };

    GetAllProducts();

});
angular.module("umbraco").controller('flxFreeProduct.Controller.Edit', function ($scope, flxCatalogueResource, $filter) {

    // *****Scope variables*****

    $scope.Validate = function (reward, type) {
        if (flxFreeProductValidation(reward)) {
            $scope.EditReward(reward, type)
        }
    }

    $scope.loading = true;

    function GetAllProducts() {
        flxCatalogueResource.getAllProducts().then(function (response) {
            if (response.data.length > 0) {
                $scope.products = response.data;
                $scope.loading = false;
            }
            else {
                $scope.loading = false;
            }
        });
    };
    function GetProductById(productId) {
        flxCatalogueResource.getProductById(productId).then(function (response) {
            if (response.data != null) {
                $scope.product = response.data;
                $scope.loading = false;
            }
            else {
                $scope.loading = false;
            }
        });
    };

    GetAllProducts();

    if ($scope.reward != null) {
        //If true, user is on Edit reward view
        GetProductById($scope.reward.ProductID);
    }

});

function flxFreeProductValidation(reward) {

    if (reward.Name == null || !reward.Name) {
        alert("Please complete the Name field");
        return false;
    };
    if (reward.Starts == null || !reward.Starts) {
        alert("Please complete the Starts field");
        return false;
    };
    if (reward.Starts == null || !reward.Starts) {
        alert("Please enter a date and a time in the Starts field");
        return false;
    };
    if (reward.Ends == null || !reward.Ends) {
        alert("Please enter a date and a time in the Ends field");
        return false;
    };
    if (reward.Quantity == null || reward.Quantity == 0) {
        alert("Quantity value must be greater than zero");
        return false;
    };

    return true;

};
