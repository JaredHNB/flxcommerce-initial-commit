﻿angular.module("umbraco").controller('flxPercentOffOrder.Controller.Add', function ($scope, flxCatalogueResource, userService, $filter) {

    // *****Scope variables*****
    $scope.newReward = {
        Name: null,
        Starts: null,
        Ends: null,
        DiscountPercent: null
    };

    $scope.Validate = function (reward, type) {
        if (flxPercentOffOrderValidation(reward)) {
            $scope.AddReward(reward, type)
        }
    }


});
angular.module("umbraco").controller('flxPercentOffOrder.Controller.Edit', function ($scope, flxCatalogueResource, userService, $filter) {

    // *****Scope variables*****
    $scope.reward.DiscountPercent = $filter('number')($scope.reward.DiscountPercent, '2');

    $scope.Validate = function (reward, type) {
        if (flxPercentOffOrderValidation(reward)) {
            $scope.EditReward(reward, type)
        }
    }

    $scope.Validate = function (reward, type) {
        if (flxPercentOffOrderValidation(reward)) {
            $scope.EditReward(reward, type)
        }
    }

});

function flxPercentOffOrderValidation(reward) {

    var decimalRegex = /^\d+(\.\d{1,2})?$/;

    if (reward.Name == null || !reward.Name) {
        alert("Please complete the Name field");
        return false;
    };
    if (reward.Starts == null || !reward.Starts) {
        alert("Please enter a date and a time in the Starts field");
        return false;
    };
    if (reward.Ends == null || !reward.Ends) {
        alert("Please enter a date and a time in the Ends field");
        return false;
    };
    if (reward.DiscountPercent == null || !decimalRegex.test(reward.DiscountPercent)) {
        alert("Please enter a valid discount e.g. 25 or 25.50");
        return false;
    };

    return true;

};
