﻿angular.module("umbraco").controller('flxPromotions.Controller', function ($scope, flxPromotionsResource, userService, $filter) {

    // *****Scope variables*****
    $scope.promotionTemplates = new Array();
    $scope.promotionTemplates["promotions"] = "/App_Plugins/flxCommerceApplication/backoffice/flxPromotion/promotions/views/promotions.html";
    $scope.promotionTemplates["addPromotion"] = "/App_Plugins/flxCommerceApplication/backoffice/flxPromotion/promotions/views/addPromotion.html";
    $scope.promotionTemplates["editPromotion"] = "/App_Plugins/flxCommerceApplication/backoffice/flxPromotion/promotions/views/editPromotion.html";

    $scope.promotionTemplate = null;

    $scope.promotions = new Array();
    $scope.promotion = null;
    $scope.newPromotion = {
        Name: null,
        isEnabled: false
    };

    $scope.loading = true;

    userService.getCurrentUser().then(function (user) {
        $scope.user = user;
    });

    $scope.SelectPromotionTemplate = function (index) {

        if ($scope.promotionTemplates[index] != null) {
            $scope.promotionTemplate = $scope.promotionTemplates[index];
        }
        else {
            $scope.promotionTemplate = null;
        }

    };

    $scope.SelectPromotion = function (promotion, template) {

        for (var i = 0; i < $scope.promotions.length; i++) {
            $scope.promotions[i].IsSelected = false;
        }

        $scope.promotion = promotion;
        $scope.promotion.IsSelected = true;
        $scope.SelectPromotionTemplate(template);

    };


    $scope.AddPromotion = function (newPromotion) {

        if (flxPromotionsValidation(newPromotion)) {
            newPromotion.CreatedBy = $scope.user.name;
            newPromotion.CreatedOn = $filter('date')(Date.now(), 'yyyy-MM-dd HH:mm:ss', null);
            newPromotion.MarketingCampaignId = $scope.campaign.MarketingCampaignId;

            flxPromotionsResource.createPromotion(newPromotion).then(function (response) {
                $scope.loading = true;
                GetPromotions($scope.campaign.MarketingCampaignId);
                $scope.SelectPromotionTemplate('promotions');
            })
           
       };
    };

    $scope.EditPromotion = function (promotion) {

        if (flxPromotionsValidation(promotion)) {
            promotion.ModifiedBy = $scope.user.name;
            promotion.ModifiedOn = $filter('date')(Date.now(), 'yyyy-MM-dd HH:mm:ss', null);

            flxPromotionsResource.updatePromotion(promotion).then(function () {
                $scope.loading = true;
                GetPromotions($scope.campaign.MarketingCampaignId);
                $scope.SelectPromotionTemplate('promotions');
            });
        }

    };

    $scope.DeletePromotion = function (promotion) {
        if (confirm('Are you sure you want to delete this promotion?') == true) {
            $scope.loading = true;

            //Delete Goals and Rewards


            flxPromotionsResource.deletePromotion(promotion).then(function () {
                GetPromotions($scope.campaign.MarketingCampaignId);
                $scope.SelectPromotionTemplate('promotions');
            });
        }
    };

    function GetPromotions(campaignId) {
        flxPromotionsResource.getAllPromotionsByMarketingCampaignId(campaignId).then(function (response) {
            if (response.data.length > 0) {
                $scope.promotions = response.data;

                //Select first promotion from list
                //$scope.SelectPromotion($scope.promotions[0], 'promotions');

                $scope.noData = false;
                $scope.loading = false;
            }
            else {
                $scope.noData = true;
                $scope.loading = false;
            }
        });
    }

    function selectDefaultTemplate() {
        $scope.promotionTemplate = $scope.promotionTemplates["promotions"];
    };

    function flxPromotionsValidation(promotion) {

        if (promotion.Name == null || !promotion.Name) {
            alert("Please complete the Name field");
            return false;
        };

        return true;

    };


    selectDefaultTemplate();
    GetPromotions($scope.campaign.MarketingCampaignId);
});