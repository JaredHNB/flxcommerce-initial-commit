﻿angular.module("umbraco").controller('flxCampaigns.Controller', function ($scope, flxCampaignsResource, userService, $location, $filter) {

    $scope.loading = true;

    // *****Scope variables*****
    $scope.SideTemplates = new Array();
    $scope.SideTemplates["campaign"] = "/App_Plugins/flxCommerceApplication/backoffice/flxPromotion/campaigns/views/campaign.html";
    $scope.SideTemplates["addCampaign"] = "/App_Plugins/flxCommerceApplication/backoffice/flxPromotion/campaigns/views/addCampaign.html";
    $scope.SideTemplates["editCampaign"] = "/App_Plugins/flxCommerceApplication/backoffice/flxPromotion/campaigns/views/editCampaign.html";

    $scope.SideTemplate = null;

    $scope.campaigns = new Array();
    $scope.campaign = null;
    $scope.newCampaign = {
        Name: null,
        Starts: null,
        Ends: null,
        isEnabled: false
    };

    userService.getCurrentUser().then(function (user) {
        $scope.user = user;
    });


    $scope.SelectSideTemplate = function (index) {

        if ($scope.SideTemplates[index] != null) {
            $scope.SideTemplate = $scope.SideTemplates[index];
        }
        else {
            $scope.SideTemplate = null;
        }
    }

    $scope.SelectCampaign = function (campaign, template) {

        for (var i = 0; i < $scope.campaigns.length; i++) {
            $scope.campaigns[i].IsSelected = false;
        }


        $scope.campaign = campaign;
        $scope.campaign.IsSelected = true;

        $scope.SelectSideTemplate(template);
    };


    $scope.AddCampaign = function (newCampaign) {

        if (flxCampaignsValidation(newCampaign)) {
            newCampaign.CreatedBy = $scope.user.name;
            newCampaign.CreatedOn = $filter('date')(Date.now(), 'yyyy-MM-dd HH:mm:ss', null);

            flxCampaignsResource.createMarketingCampaign(newCampaign).then(function (response) {
                $scope.loading = true;
                GetCurrentCampaigns();
                $scope.SelectSideTemplate(0);
            })
        }
    };
    $scope.EditCampaign = function (campaign) {

        if (flxCampaignsValidation(campaign)) {
            campaign.ModifiedBy = $scope.user.name;
            campaign.ModifiedOn = $filter('date')(Date.now(), 'yyyy-MM-dd HH:mm:ss', null);

            flxCampaignsResource.updateMarketingCampaign(campaign).then(function (response) {
                $scope.loading = true;
                GetCurrentCampaigns();
                $scope.SelectSideTemplate(0);
            })
        }
    };
    $scope.DeleteCampaign = function (campaign) {
        if (confirm('Are you sure you want to delete this campaign?') == true) {
            flxCampaignsResource.deleteMarketingCampaign(campaign).then(function () {
                $scope.loading = true;
                GetCurrentCampaigns();
                $scope.SelectSideTemplate(0);
            });
        }
    };

    function GetCurrentCampaigns() {
        flxCampaignsResource.getAllCurrentMarketingCampaigns().then(function (response) {
            if (response.data.length > 0) {
                $scope.campaigns = response.data;
                $scope.noData = false;
                $scope.loading = false;
            }
            else {
                $scope.noData = true;
                $scope.loading = false;

            }
        });
    }

    function flxCampaignsValidation(campaign) {

        if (campaign.Name == null || !campaign.Name) {
            alert("Please complete the Name field");
            return false;
        };
        if (campaign.Starts == null || !campaign.Starts) {
            alert("Please complete the Starts field");
            return false;
        };
        if (campaign.Starts == null || !campaign.Starts) {
            alert("Please enter a date and a time in the Starts field");
            return false;
        };

        return true;

    };

    GetCurrentCampaigns();
});