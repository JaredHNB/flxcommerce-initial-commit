﻿angular.module("umbraco").controller('flxCouponCode.Controller.Add', function ($scope) {

    // *****Scope variables*****
    $scope.newGoal = {
        Name: null,
        Starts: null,
        Ends: null,
        Code: null
    };

    $scope.Validate = function (newGoal, type) {
        if (flxCouponCodeValidation(newGoal)) {
            $scope.AddGoal(newGoal, type)
        }
    }

});
angular.module("umbraco").controller('flxCouponCode.Controller.Edit', function ($scope) {

    // *****Scope variables*****

    $scope.Validate = function (goal, type) {
        if (flxCouponCodeValidation(goal)) {
            $scope.EditGoal(goal, type)
        }
    }

});

function flxCouponCodeValidation(goal) {

    if (goal.Name == null || !goal.Name) {
        alert("Please complete the Name field");
        return false;
    };
    if (goal.Starts == null || !goal.Starts) {
        alert("Please complete the Starts field");
        return false;
    };
    if (goal.Starts == null || !goal.Starts) {
        alert("Please enter a date and a time in the Starts field");
        return false;
    };
    if (goal.Code == null || !goal.Code) {
        alert("Please complete the Code field");
        return false;
    };

    return true;

};
