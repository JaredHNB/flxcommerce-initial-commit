﻿angular.module("umbraco").controller('flxSpendMoreThan.Controller.Add', function ($scope) {

    // *****Scope variables*****
    $scope.newGoal = {
        Name: null,
        Starts: null,
        Ends: null,
        Amount: null
    };

    $scope.Validate = function (newGoal, type) {
        if (flxSpendMoreThanValidation(newGoal)) {
            $scope.AddGoal(newGoal, type)
        }
    }

});
angular.module("umbraco").controller('flxSpendMoreThan.Controller.Edit', function ($scope) {

    // *****Scope variables*****

    $scope.Validate = function (goal, type) {
        if (flxSpendMoreThanValidation(goal)) {
            $scope.EditGoal(goal, type)
        }
    }

});

function flxSpendMoreThanValidation(goal) {

    var decimalRegex = /^\d+(\.\d{1,2})?$/;

    if (goal.Name == null || !goal.Name) {
        alert("Please complete the Name field");
        return false;
    };
    if (goal.Starts == null || !goal.Starts) {
        alert("Please complete the Starts field");
        return false;
    };
    if (goal.Starts == null || !goal.Starts) {
        alert("Please enter a date and a time in the Starts field");
        return false;
    };
    if (goal.Amount == null || !goal.Amount) {
        alert("Please enter a valid Amount e.g. 25 or 25.50");
        return false;
    };

    return true;

};
