﻿angular.module("umbraco").controller('flxIsNotOfRole.Controller.Add', function ($scope, flxGoalsResource) {

    // *****Scope variables*****
    $scope.newGoal = {
        Name: null,
        Starts: null,
        Ends: null,
        Role: null
    };
    $scope.Roles = {};
    flxGoalsResource.getAllRoles().then(function (response) {
        $scope.Roles = response.data;
    });

    $scope.Validate = function (newGoal, type) {
        if (flxIsNotOfRoleValidation(newGoal)) {
            $scope.AddGoal(newGoal, type)
        }
    }

});
angular.module("umbraco").controller('flxIsNotOfRole.Controller.Edit', function ($scope, flxGoalsResource) {

    // *****Scope variables*****

    $scope.Validate = function (goal, type) {
        if (flxIsNotOfRoleValidation(goal)) {
            $scope.EditGoal(goal, type)
        }
    }
    $scope.Roles = {};
    flxGoalsResource.getAllRoles().then(function (response) {
        $scope.Roles = response.data;
    });

});

function flxIsNotOfRoleValidation(goal) {

    if (goal.Name == null || !goal.Name) {
        alert("Please complete the Name field");
        return false;
    };
    if (goal.Starts == null || !goal.Starts) {
        alert("Please complete the Starts field");
        return false;
    };
    if (goal.Starts == null || !goal.Starts) {
        alert("Please enter a date and a time in the Starts field");
        return false;
    };
    if (goal.Role == null || !goal.Role) {
        alert("Please complete the Role field");
        return false;
    };

    return true;

};
