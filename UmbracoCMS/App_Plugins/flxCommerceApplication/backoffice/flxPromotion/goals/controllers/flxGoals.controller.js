﻿angular.module("umbraco").controller('flxGoals.Controller', function ($scope, flxGoalsResource, flxGoalCollection, userService, $filter) {

    // *****Scope variables*****
    $scope.goalTemplates = new Array();
    $scope.goalTemplates["goals"] = "/App_Plugins/flxCommerceApplication/backoffice/flxPromotion/goals/views/goals.html";
    $scope.goalTemplates["goalCollection"] = "/App_Plugins/flxCommerceApplication/backoffice/flxPromotion/goals/views/goalCollection.html";
    $scope.goalTemplates["addCouponCode"] = "/App_Plugins/flxCommerceApplication/backoffice/flxPromotion/goals/views/goals/addCouponCode.html";
    $scope.goalTemplates["editCouponCode"] = "/App_Plugins/flxCommerceApplication/backoffice/flxPromotion/goals/views/goals/editCouponCode.html";
    $scope.goalTemplates["addSpendMoreThan"] = "/App_Plugins/flxCommerceApplication/backoffice/flxPromotion/goals/views/goals/addSpendMoreThan.html";
    $scope.goalTemplates["editSpendMoreThan"] = "/App_Plugins/flxCommerceApplication/backoffice/flxPromotion/goals/views/goals/editSpendMoreThan.html";
    $scope.goalTemplates["addIsOfRole"] = "/App_Plugins/flxCommerceApplication/backoffice/flxPromotion/goals/views/goals/addIsOfRole.html";
    $scope.goalTemplates["editIsOfRole"] = "/App_Plugins/flxCommerceApplication/backoffice/flxPromotion/goals/views/goals/editIsOfRole.html";
    $scope.goalTemplates["addIsNotOfRole"] = "/App_Plugins/flxCommerceApplication/backoffice/flxPromotion/goals/views/goals/addIsNotOfRole.html";
    $scope.goalTemplates["editIsNotOfRole"] = "/App_Plugins/flxCommerceApplication/backoffice/flxPromotion/goals/views/goals/editIsNotOfRole.html";

    $scope.goalTemplate = null;

    $scope.goals = new Array();
    $scope.goal = null;
    $scope.newGoal = null;

    $scope.goalCollection = flxGoalCollection.getCollection();

    $scope.loading = true;

    userService.getCurrentUser().then(function (user) {
        $scope.user = user;
    });

    $scope.SelectGoalTemplate = function (index) {

        if ($scope.goalTemplates[index] != null) {
            $scope.goalTemplate = $scope.goalTemplates[index];
        }
        else {
            $scope.goalTemplate = null;
        }

    }

    $scope.SelectGoal = function (goal, template) {
        $scope.goal = goal;
        $scope.SelectGoalTemplate(template);

    };

    $scope.AddGoal = function (newGoal, type) {

        newGoal.CreatedBy = $scope.user.name;
        newGoal.CreatedOn = $filter('date')(Date.now(), 'yyyy-MM-dd HH:mm:ss', null);
        newGoal.ModifiedBy = null;
        newGoal.ModifiedOn = null;
        newGoal.Type = type;
        newGoal.PromotionGoalID = 0;
        newGoal.PromotionId = $scope.promotion.PromotionId;

        flxGoalsResource.createGoal(newGoal).then(function (response) {
            $scope.loading = true;
            GetGoalsByPromotionId($scope.promotion.PromotionId);
            $scope.SelectGoalTemplate('goals');
        })
    };

    $scope.EditGoal = function (goal, type) {

        goal.ModifiedBy = $scope.user.name;
        goal.ModifiedOn = $filter('date')(Date.now(), 'yyyy-MM-dd HH:mm:ss', null);
        goal.Type = type;

        flxGoalsResource.updatePromotionGoal(goal).then(function () {
            $scope.loading = true;
            GetGoalsByPromotionId($scope.promotion.PromotionId);
            $scope.SelectGoalTemplate('goals');
        });
    };

    $scope.DeleteGoal = function (goal, type) {
        if (confirm('Are you sure you want to delete this promotion?') == true) {
            goal.Type = type;

            flxGoalsResource.deletePromotionGoal(goal).then(function () {
                $scope.loading = true;
                GetGoalsByPromotionId($scope.promotion.PromotionId);
                $scope.SelectGoalTemplate('goals');
            });
        }
    };

    //$scope.$on('DeleteGoal', function (e) {

    //});

    function GetGoalsByPromotionId(promotionId) {
        flxGoalsResource.getAllGoalsByPromotionId(promotionId).then(function (response) {
            if (response.data.length > 0) {
                $scope.goals = response.data;

                $scope.noData = false;
                $scope.loading = false;
            }
            else {
                $scope.goals = null;
                $scope.noData = true;
                $scope.loading = false;

            }
        });
    }

    function selectDefaultTemplate() {
        $scope.goalTemplate = $scope.goalTemplates["goals"];
    };

    selectDefaultTemplate();

    $scope.$watch('promotion', function (newVal, oldVal) {
        if ($scope.promotion != null) {
            $scope.loading = true;
            $scope.goals = null;
            GetGoalsByPromotionId($scope.promotion.PromotionId);

        }
    });

});