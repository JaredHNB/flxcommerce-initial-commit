﻿angular.module("umbraco").controller("flx.optionSelector",
    function ($scope, contentResource, editorState) {


        contentResource.getChildren($scope.model.config.parentOptionId, null).then(function (response) {
            var options = response;
            updateView(options.items, $scope.model.value);

        });

        $scope.$watch("SelectedItems", function (newVal, oldVal) {

            if (newVal != oldVal) {
                $scope.model.value = [];
                if (newVal) {
                    for (var i = 0; i < newVal.length; i++) {
                        if (newVal[i].isChecked) {
                            $scope.model.value.push(newVal[i]);
                        }
                    }
                }
            }

        }, true);

        //Keeps check box choices checked after save and publish
        function updateView(options, umbracoData) {
            $scope.SelectedItems = [];

            if (options != undefined) {
                for (var x = 0; x < options.length; x++) {

                    var isChecked = isDefault(options[x]);

                    if (umbracoData != undefined && !isChecked) {
                        //Test if current product is linked to current category
                        isChecked = isOptionChecked(options[x], umbracoData);
                    }

                    $scope.SelectedItems.push({
                        isChecked: isChecked,
                        Key: options[x].id,
                        Val: options[x].name,
                        ProductId: editorState.current.id
                    });

                }

            }
        }

        function isOptionChecked(option, umbracoData) {
            var index = findIndex(option, umbracoData);
            if (index != -1) {
                return true;
            }
            else {
                return false;
            }
        }

        //Returns the index of an array
        function findIndex(item, arr) {
            var index = -1;
            for (var x = 0; x < arr.length; x++) {
                if (arr[x].Key == item.id) {
                    index = x;
                    return index;
                }
            }

            return index;

        };


        function isDefault(option) {
            if (setDefaults(option) != "" && setDefaults(option) != "0") {
                return true;
            }
            else { return false; }
        };

        function setDefaults(option) {
            var IsDefaultOption = "";

            for (var i = 0; i < option.properties.length; i++) {
                if (option.properties[i].alias == "flxIsDefaultOption") {
                    IsDefaultOption = option.properties[i].value
                }
            }

            return IsDefaultOption;
        }

    });







