﻿angular.module("umbraco").controller("flx.categorySelectorController",
    function ($scope, $http, $q, editorState) {

        //Make Category scope an array
        $scope.Categories = new Array();

        var def1 = $q.defer();
        var def2 = $q.defer();

        def1.promise.then(function (data) {

            //Add api data to UmbracoRelationData scope
            $scope.UmbracoRelationData = data;

            //Get Category Data from api
            $http({ method: "Get", url: "Api/Catalogue/GetAllCategories" })
                .success(function (data) {
                    def2.resolve(data);

                })
                .error(function () {
                    $scope.error = "Property editor failed to load";
                })

        });

        def2.promise.then(function (data) {
            //Add api data to Category scope
            for (var x = 0; x < data.length; x++) {
                $scope.Categories.push({ Info: data[x], Selected: false });
            }

            $scope.CategoryName = data.Name;

            //Keeps check box choices checked after save and publish
            updatePropertyView($scope.Categories, $scope.UmbracoRelationData);
        });


        //Get Relations Data from api
        $http({ method: "Get", url: "Api/Catalogue/GetAllUmbracoRelations" })
            .success(function (data) {
                def1.resolve(data);
            })
            .error(function () {
                $scope.error = "Property editor failed to load";
            })

        //Listen for any changes to the scope i.e. changes in the checked state of checkboxes
        $scope.$watch("Categories", function (newVal, oldVal) {
            $scope.model.value = new Array();

            for (var x = 0; x < $scope.Categories.length; x++) {
                $scope.model.value.push({ CategoriesId: $scope.Categories[x].Info.CategoryID, Selected: $scope.Categories[x].Selected })
            }


        }, true);

        //Keeps check box choices checked after save and publish
        function updatePropertyView(Categories, UmbracoRelationData) {

            for (var x = 0; x < UmbracoRelationData.length; x++) {
                //Test if current Category is linked to current category
                if (UmbracoRelationData[x].ParentId == editorState.current.id) {
                    var index = findIndex(Categories, UmbracoRelationData[x].ChildId);
                    if (index != -1) {
                        $scope.Categories[index].Selected = true;
                    }
                }
            }
        }

        //Returns the index of an array
        function findIndex(arr, item) {
            var index = -1;
            for (var x = 0; x < arr.length; x++) {
                if (arr[x].Info.CategoryID == item) {
                    index = x;
                    return index;
                }
            }

            return index;

        }

    });







