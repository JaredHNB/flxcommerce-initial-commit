UmbracoCms - Compiled 29/01/19 Umbraco version 7.13.1 assembly: 1.0.6949.28284

Use NuGet for all updates.

Admin access:
User:  developer
Email: administrator@leekes.co.uk
Pass:  #Se6Nyy?/R

When upgrading Umbraco via NuGet
    Non standard Image Grid Datatype (Rhys)

    If you upgrade Umbraco the grid view editor may/will be overwriten. Add the following to this file:
    Umbraco\Views\propertyeditors\grid\editors\media.html

    Line    Code
    13      <input type="text" class="caption" ng-model="control.value.caption" localize="placeholder" placeholder="@grid_placeholderImageCaption" />
    14      <input type="text" class="caption" ng-model="control.value.link" localize="placeholder" placeholder="Image Link..." />
    