﻿using Leekes.Umbraco.KeyValueExtension.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace Leekes.Umbraco.KeyValueExtension.Extensions
{
	public static class HtmlHelperExtensions
	{
		public static MvcHtmlString KeyValueExt(this HtmlHelper helper, Models.KeyValueExt opengraph)
		{
			return helper.KeyValueExt(opengraph.ToList());
		}

		public static MvcHtmlString KeyValueExt(this HtmlHelper helper, string propertyName, Models.KeyValueExt opengraph)
		{
			var curPage = GetCurrentPage();
			return helper.KeyValueExt(curPage, propertyName, opengraph.ToList());
		}

		public static MvcHtmlString KeyValueExt(this HtmlHelper helper,
			IEnumerable<dynamic> defaultMetadata = null)
		{
			var curPage = GetCurrentPage();
			var cacheName = string.Join("_", nameof(HtmlHelperExtensions), nameof(KeyValueExt),
				curPage.DocumentTypeAlias);

			var currentCachedPropertyName = (string)ApplicationContext.Current
				.ApplicationCache
				.RuntimeCache
				.GetCacheItem(cacheName);

			//clear if there is previous cache but property is gone
			if (!string.IsNullOrWhiteSpace(currentCachedPropertyName) &&
				!curPage.HasProperty(currentCachedPropertyName))
				ApplicationContext.Current
					.ApplicationCache
					.RuntimeCache
					.ClearCacheItem(cacheName);

			//retry if no cache available or if previous cached property does not exist
			if (string.IsNullOrWhiteSpace(currentCachedPropertyName)
				|| !curPage.HasProperty(currentCachedPropertyName))
				currentCachedPropertyName = (string)ApplicationContext.Current
					.ApplicationCache
					.RuntimeCache
					.GetCacheItem(cacheName, () => GetPropertyName(curPage));

			return helper.KeyValueExt(curPage, currentCachedPropertyName, defaultMetadata);
		}

		public static MvcHtmlString KeyValueExt(this HtmlHelper helper, string propertyName,
			IEnumerable<dynamic> defaultMetadata = null)
		{
			var curPage = GetCurrentPage();
			return helper.KeyValueExt(curPage, propertyName, defaultMetadata);
		}

		private static MvcHtmlString KeyValueExt(this HtmlHelper helper,
			IPublishedContent content,
			string propertyName,
			IEnumerable<dynamic> defaultMetadata = null)
		{
			var metaData = !string.IsNullOrWhiteSpace(propertyName) && content.HasProperty(propertyName)
				? content.GetPropertyValue<List<KeyValueMetaData>>(propertyName)
				: new List<KeyValueMetaData>();

			var defaultMeta = defaultMetadata
				?.Select(m => JsonConvert.DeserializeObject<KeyValueMetaData>(JsonConvert.SerializeObject(m) as string))
				.WhereNotNull()
				.Where(d => !metaData.Any(m =>
					m.Key.Equals(d.Key, StringComparison.InvariantCultureIgnoreCase)))
				.ToList();

			if (defaultMeta != null) metaData.AddRange(defaultMeta);

			var htmlString = metaData
				.Select(m => $@"<dt>{m.Key}</dt><dd>{HttpUtility.HtmlEncode(m.Value)}</dd>")
				.ToList();


			return MvcHtmlString.Create(string.Join(string.Empty, "<dl>" + htmlString + "</dl>"));
		}

		/*public static string KeyValueDataList(IEnumerable<dynamic> keyValueData)
		{
			var metaData = new List<KeyValueMetaData>();
			var defaultMeta = keyValueData//.Replace("\"metadata\": ", "").TrimStart("{").TrimEnd("}")
					?.Select(m => JsonConvert.DeserializeObject<KeyValueMetaData>(JsonConvert.SerializeObject(m) as string))
					.WhereNotNull()
					.Where(d => !metaData.Any(m =>
						m.Key.Equals(d.Key, StringComparison.InvariantCultureIgnoreCase)))
					.ToList();

			if (defaultMeta != null) metaData.AddRange(defaultMeta);

			var htmlString = metaData
				.Select(m => $@"<dt>{m.Key}</dt><dd>{HttpUtility.HtmlEncode(m.Value)}</dd>")
				.ToList();


			return string.Join(string.Empty, "<dl>" + htmlString + "</dl>");

		}*/
		public static List<KeyValueMetaDataInformation> KeyValueMetaList(Object keyValueData)
		{
			var defaultMeta = keyValueData;

			var outString = defaultMeta.ToString().Replace(Environment.NewLine, "").Replace("\\t", "");

			KeyValueMetaInformation convert = JsonConvert.DeserializeObject<KeyValueMetaInformation>(outString.ToString());

			return convert.metadata;
		}
		public static string KeyValueDataList(Object keyValueData)
		{
			var convert = KeyValueMetaList(keyValueData);

			var htmlString = new StringBuilder();
			foreach (KeyValueMetaDataInformation m in convert)
			{
				htmlString.AppendFormat("<dt>{0}</dt><dd>{1}</dd>", m.key, m.value);
			}

			return string.Join(string.Empty, "<dl>" + htmlString + "</dl>");
		}

		private static string GetPropertyName(IPublishedContent content)
		{
			return content.Properties
				.FirstOrDefault(p => p.GetValue<List<KeyValueMetaData>>() != null)
				?.PropertyTypeAlias;
		}

		/*private static IEnumerable<KeyValueMetaData> GetPropertyName(IPublishedContent content)
		{
			return content.Properties
				.Select(p => p.GetValue<List<KeyValueMetaData>>());
		}*/

		private static IPublishedContent GetCurrentPage()
		{
			var umbHelper = new UmbracoHelper(UmbracoContext.Current);
			return umbHelper.UmbracoContext.PublishedContentRequest.PublishedContent;
		}
	}
}
