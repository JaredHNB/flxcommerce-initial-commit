﻿using Leekes.Umbraco.KeyValueExtension.Models;
using System.Collections.Generic;
using Umbraco.Web;

namespace Leekes.Umbraco.KeyValueExtension.Extensions
{
	public static class KeyValueExtensions
	{
		public static IEnumerable<KeyValueMetaData> ToList(this Models.KeyValueExt keyvaluedata)
		{
			var umbHelper = new UmbracoHelper(UmbracoContext.Current);
			var model = new List<KeyValueMetaData>();

			if (keyvaluedata == null) return model;

			model.AddRange(keyvaluedata.Metadata);
			return model;
		}
	}
}
