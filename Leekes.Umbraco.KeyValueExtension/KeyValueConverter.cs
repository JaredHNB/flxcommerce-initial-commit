﻿using Leekes.Umbraco.KeyValueExtension.Extensions;
using Leekes.Umbraco.KeyValueExtension.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.PropertyEditors;

namespace Leekes.Umbraco.KeyValueExtension
{
	[PropertyValueType(typeof(IEnumerable<KeyValueMetaData>))]
	[PropertyValueCache(PropertyCacheValue.All, PropertyCacheLevel.Content)]
	public class KeyValueConverter : IPropertyValueConverter
	{
		public bool IsConverter(PublishedPropertyType propertyType)
		{
			return propertyType.PropertyEditorAlias.Equals("keyValue", StringComparison.InvariantCultureIgnoreCase);
		}

		public object ConvertDataToSource(PublishedPropertyType propertyType, object source, bool preview)
		{
			return Convert.ToString(source);
		}

		public object ConvertSourceToObject(PublishedPropertyType propertyType, object source, bool preview)
		{
			var opengraph = JsonConvert.DeserializeObject<Models.KeyValueExt>(source as string);
			return opengraph == null ? new List<KeyValueMetaData>() : opengraph.ToList();
		}

		public object ConvertSourceToXPath(PublishedPropertyType propertyType, object source, bool preview)
		{
			throw new NotImplementedException();
		}
	}
}
