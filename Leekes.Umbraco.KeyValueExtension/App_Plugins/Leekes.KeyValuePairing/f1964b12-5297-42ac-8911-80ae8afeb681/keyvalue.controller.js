﻿angular.module('umbraco')
    .controller('Leekes.Umbraco.KeyValueExtension.KeyValueConverter', function ($scope, $element, assetsService, dialogService, entityResource) {
        

        function init() {
            $scope.model.hideLabel = true;
            initKeyValueModels();

            if (!$scope.model.value) {
                $scope.model.value = {};
            }

            if (!$scope.model.value.metadata) {
                $scope.model.value.metadata = [];
            }
        }

        function initKeyValueModels() {
            $scope.keyvalue = {};
            $scope.keyvalue.types = {
                "music.song": "Music: Song",
                "music.album": "Music: Album",
                "music.playlist": "Music: Playlist",
                "music.radio_station": "Music: Radio Station",
                "video.movie": "Video: Movie",
                "video.episode": "Video: Episode",
                "video.tv_show": "Video: Tv Show",
                "video.other": "Video: Other",
                "article": "Article",
                "book": "Book",
                "profile": "Profile",
                "website": "Website",
                "blog": "Blog",
                "game": "Game",
                "movie": "Movie",
                "food": "Food",
                "city": "City",
                "country": "Country",
                "company": "Company",
                "hotel": "Hotel",
                "restaurant": "Restaurant",
            };
            
            $scope.metadata = {
                key: '',
                value: ''
            };
        }
        function initThumbnailModel() {
        }

        $scope.removeMetadata = function(index) {
            delete $scope.model.value.metadata[index];
            $scope.model.value.metadata = $scope.model.value.metadata.filter(Boolean);

        }

        $scope.addMetadata = function() {
            $scope.model.value.metadata.push({
                "key": $scope.metadata.key,
                "value": $scope.metadata.value
            });
            $scope.metadata.key = '';
            $scope.metadata.value = '';
        }
        init();
    });