﻿using Newtonsoft.Json;
namespace Leekes.Umbraco.KeyValueExtension.Models
{
	public class KeyValueMetaData
	{
		[JsonProperty("key")]
		public string Key { get; set; }

		[JsonProperty("value")]
		public string Value { get; set; }
	}
}
