﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Leekes.Umbraco.KeyValueExtension.Models
{
	public class KeyValueExt
	{
		[JsonProperty("metadata")]
		public IEnumerable<KeyValueMetaData> Metadata { get; set; }
	}
}
