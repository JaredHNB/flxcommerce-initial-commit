﻿using System.Collections.Generic;

namespace Leekes.Umbraco.KeyValueExtension.Models
{
	public class KeyValueMetaInformation
	{
		public List<KeyValueMetaDataInformation> metadata { get; set; }

	}
	public class KeyValueMetaDataInformation
	{
		public string key { get; set; }
		public string value { get; set; }

	}
}
